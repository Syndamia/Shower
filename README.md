# Shower

This repository is for projects that aren't big enough to have their own repository but also aren't small enough to be considered part of the [Self-learning](https://github.com/Syndamia/Self-learning) repo.
