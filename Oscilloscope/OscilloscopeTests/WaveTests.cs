﻿using System;
using System.Linq;
using Oscilloscope;
using NUnit.Framework;
using System.Collections.Generic;

namespace OscilloscopeTests {
    [TestFixture]
    public class WaveTests {

        [Test]
        public static void InitialisationOverloadsNameCreaton() {
            Wave wave1 = new Wave();
            Wave wave2 = new Wave("My wave");

            Assert.AreEqual("Wave1", wave1.Name, "Automatic wave name isn't working properly.");
            Assert.AreEqual("My wave", wave2.Name, "Custom wave name from overload doesn't work correctly.");

            var testException = Assert.Throws<ArgumentException>(() => new Wave(""));
            Assert.AreEqual("Name cannot be empty string!", testException.Message, "There is no eception if you input an empty string as a name");
        }

        [Test]
        public static void InitialisationWithAListOfValues() {
            var testValues = new List<int>() { int.MinValue, int.MinValue + 1, 0, int.MaxValue - 1, int.MaxValue };
            Wave testWave = new Wave(new List<int>() { int.MinValue, int.MinValue + 1, 0, int.MaxValue - 1, int.MaxValue});

            for (int i = 0; i < 5; i++) {
                Assert.AreEqual(testValues[i], testWave.GetValue(), $"Wave doesn't return right value (failed on value No {i})");
            }
            Assert.AreEqual(testValues[0], testWave.GetValue(), "Wave doesn't reset return index variable correctly.");
        }
    }
}
