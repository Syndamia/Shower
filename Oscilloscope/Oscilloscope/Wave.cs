﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oscilloscope
{
    public class Wave
    {
        private static int waveCount = 0;

        private string name;

        private List<int> PeriodValues { get; set; } //a period is compiled of all the values that don't repeat, periods are put next to one another to form the whole wave ; they are the base of the wave
        private int CurrValue { get; set; }

        public Wave() : this($"Wave{++waveCount}", new List<int>()) 
        { }

        public Wave(string name) : this(name, new List<int>()) 
        { }

        public Wave(List<int> periodValues) :this($"Wave{++waveCount}", periodValues) 
        { }

        public Wave(string name, List<int> periodValues) {
            this.Name = name;
            this.PeriodValues = periodValues;

            this.CurrValue = 0;
            waveCount++;
        }

        public int GetValue() {
            if (CurrValue == PeriodValues.Count()) CurrValue = 0;

            return PeriodValues[CurrValue++];
        }

        public string Name {
            get { return this.name; }
            set {
                if (value == "") {
                    throw new ArgumentException("Name cannot be empty string!");
                }

                name = value;
            }
        }
    }
}
