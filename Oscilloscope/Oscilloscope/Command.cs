﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oscilloscope {
    class Command {
        private static string[] input = new string[2];

        public static void Do() {
            Take();
            Execute();
        }

        private static void Take() {
            input = Console.ReadLine().Split(' ').ToArray();
        }

        private static void Execute() {
            switch (input[0]) {
                case "screen":
                    Screen.SetValues(input.Skip(1).ToArray());
                    break;
                case "fps":
                    Program.Fps = double.Parse(input[1]);
                    break;
            }
        }
    }
}
