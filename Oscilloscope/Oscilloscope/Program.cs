﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oscilloscope
{
    class Program
    {
        public static double Fps = 30;

        static void Main(string[] args)
        {
            Screen.SetDefaults();
            var triangle = new Wave(new List<int>() { 5, 6, 7, 8, 9, 10, 11, 12});

            while (true) {
                Screen.Measure(triangle.GetValue());
                Screen.Print();

                if (Console.KeyAvailable) {
                    Command.Do();
                }

                Thread.Sleep((int)(1000 / Fps));
                Console.Clear();
            }
        }
    }
}
