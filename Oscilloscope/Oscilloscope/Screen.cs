﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oscilloscope
{
    public class Screen
    {
        public static int Height { get; private set; }
        public static int Width { get; private set; }

        public static char VerWall { get; private set; }
        public static char HorWall { get; private set; }

        public static char RayX { get; private set; }
        public static char RayY { get; private set; }

        public static int RayXPos { get; private set; }
        public static int RayYPos { get; private set; } //it is used only in xy mode

        private static int Time { get; set; }
        private static StringBuilder[] map;

        public static void SetDefaults() {
            Height = 10;
            Width = 20;

            VerWall = '|';
            HorWall = '-';

            RayX = '*';
            RayY = '+';
            RayXPos = Width / 2;
            RayYPos = Height / 2;

            GenerateMap();
        }

        private static void GenerateMap() {
            map = new StringBuilder[Height + 2];

            map[0] = new StringBuilder(new string(HorWall, Width + 2));
            map[map.Length - 1] = new StringBuilder(new string(HorWall, Width + 2));

            for (int i = 1; i < map.Length - 1; i++) {
                map[i] = new StringBuilder(new string(' ', Width + 2));
            }
        }

        public static void SetValues(string[] command) {
            try {
                //TODO: add other commands
                switch (command[0]) {
                    case "size":
                        Height = int.Parse(command[1]); Width = int.Parse(command[2]);
                        GenerateMap();
                        break;
                    case "char":
                        if (command[1] == "x") RayX = command[2][0];
                        else if (command[1] == "y") RayY = command[2][0];
                        else if (command[1] == "vwall") VerWall = command[2][0];
                        else if (command[1] == "hwall") HorWall = command[2][0];
                        break;
                }
            } catch (Exception) {
                Console.WriteLine("ERR");
                Thread.Sleep(1000);
            }
        }

        public static void Measure(int valueX) {
            RayXPos = valueX;
        }

        public static void Measure(int valueX, int valueY) {
            RayYPos = valueY;
        }

        public static void Print() {
            CalculateFrame();

            PrintMap();
        }

        private static void CalculateFrame() {
            for (int row = 1; row < Height; row++) {

                var currRow = map[row];
                currRow[0] = VerWall; currRow[Width + 1] = VerWall;

                if (row == RayXPos) {
                    CleanColumn(Time);

                    currRow[Time] = RayX;

                    IncrementTime();
                }
                map[row] = currRow;
            }
        }

        private static void CleanColumn(int valueCol) {
            for (int i = 1; i < map.Length - 2; i++) {
                map[i][valueCol] = ' ';
            }
        }

        private static void IncrementTime()
        {
            if (Time == Width) Time = 1;
            else Time++;
        }

        private static void PrintMap() {
            for (int i = 0; i < map.Length; i++) {
                Console.WriteLine(map[i]);
            }
        }        
    }
}
