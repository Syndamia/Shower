﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTranslate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.btnCheck = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTranslation = New System.Windows.Forms.TextBox()
        Me.cbLanguage = New System.Windows.Forms.ComboBox()
        Me.cbSourceWord = New System.Windows.Forms.ComboBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lblStatusAdded = New System.Windows.Forms.Label()
        Me.lblTotalWords = New System.Windows.Forms.Label()
        Me.btnWordAdd = New System.Windows.Forms.Button()
        Me.txtWordAdd = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtWordUpdate = New System.Windows.Forms.TextBox()
        Me.cbLanguageUpdate = New System.Windows.Forms.ComboBox()
        Me.cbSelectWord = New System.Windows.Forms.ComboBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnExportPass = New System.Windows.Forms.Button()
        Me.cbRequirePassword = New System.Windows.Forms.CheckBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.lblStatusUpdate = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(263, 171)
        Me.TabControl1.TabIndex = 7
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lblStatus)
        Me.TabPage1.Controls.Add(Me.btnCheck)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txtTranslation)
        Me.TabPage1.Controls.Add(Me.cbLanguage)
        Me.TabPage1.Controls.Add(Me.cbSourceWord)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(255, 145)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Translate"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(181, 115)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(30, 13)
        Me.lblStatus.TabIndex = 13
        Me.lblStatus.Text = "ERR"
        '
        'btnCheck
        '
        Me.btnCheck.Location = New System.Drawing.Point(10, 108)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(143, 26)
        Me.btnCheck.TabIndex = 12
        Me.btnCheck.Text = "Check"
        Me.btnCheck.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(22, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = " to:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Translate:"
        '
        'txtTranslation
        '
        Me.txtTranslation.Location = New System.Drawing.Point(10, 82)
        Me.txtTranslation.Name = "txtTranslation"
        Me.txtTranslation.Size = New System.Drawing.Size(235, 20)
        Me.txtTranslation.TabIndex = 9
        '
        'cbLanguage
        '
        Me.cbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLanguage.FormattingEnabled = True
        Me.cbLanguage.Location = New System.Drawing.Point(67, 43)
        Me.cbLanguage.Name = "cbLanguage"
        Me.cbLanguage.Size = New System.Drawing.Size(178, 21)
        Me.cbLanguage.TabIndex = 8
        '
        'cbSourceWord
        '
        Me.cbSourceWord.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSourceWord.FormattingEnabled = True
        Me.cbSourceWord.Location = New System.Drawing.Point(67, 16)
        Me.cbSourceWord.Name = "cbSourceWord"
        Me.cbSourceWord.Size = New System.Drawing.Size(178, 21)
        Me.cbSourceWord.TabIndex = 7
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.lblStatusAdded)
        Me.TabPage4.Controls.Add(Me.lblTotalWords)
        Me.TabPage4.Controls.Add(Me.btnWordAdd)
        Me.TabPage4.Controls.Add(Me.txtWordAdd)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(255, 145)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Add Word"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'lblStatusAdded
        '
        Me.lblStatusAdded.Location = New System.Drawing.Point(8, 104)
        Me.lblStatusAdded.Name = "lblStatusAdded"
        Me.lblStatusAdded.Size = New System.Drawing.Size(237, 23)
        Me.lblStatusAdded.TabIndex = 3
        Me.lblStatusAdded.Text = "ERR"
        Me.lblStatusAdded.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTotalWords
        '
        Me.lblTotalWords.Location = New System.Drawing.Point(8, 10)
        Me.lblTotalWords.Name = "lblTotalWords"
        Me.lblTotalWords.Size = New System.Drawing.Size(237, 22)
        Me.lblTotalWords.TabIndex = 2
        Me.lblTotalWords.Text = "ERR"
        Me.lblTotalWords.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnWordAdd
        '
        Me.btnWordAdd.Location = New System.Drawing.Point(8, 68)
        Me.btnWordAdd.Name = "btnWordAdd"
        Me.btnWordAdd.Size = New System.Drawing.Size(237, 20)
        Me.btnWordAdd.TabIndex = 1
        Me.btnWordAdd.Text = "Add"
        Me.btnWordAdd.UseVisualStyleBackColor = True
        '
        'txtWordAdd
        '
        Me.txtWordAdd.Location = New System.Drawing.Point(8, 42)
        Me.txtWordAdd.Name = "txtWordAdd"
        Me.txtWordAdd.Size = New System.Drawing.Size(237, 20)
        Me.txtWordAdd.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lblStatusUpdate)
        Me.TabPage3.Controls.Add(Me.btnDelete)
        Me.TabPage3.Controls.Add(Me.btnUpdate)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.txtWordUpdate)
        Me.TabPage3.Controls.Add(Me.cbLanguageUpdate)
        Me.TabPage3.Controls.Add(Me.cbSelectWord)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(255, 145)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Update Word"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(67, 89)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(178, 21)
        Me.btnUpdate.TabIndex = 15
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Translation in:"
        '
        'txtWordUpdate
        '
        Me.txtWordUpdate.Location = New System.Drawing.Point(8, 63)
        Me.txtWordUpdate.Name = "txtWordUpdate"
        Me.txtWordUpdate.Size = New System.Drawing.Size(237, 20)
        Me.txtWordUpdate.TabIndex = 11
        '
        'cbLanguageUpdate
        '
        Me.cbLanguageUpdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLanguageUpdate.FormattingEnabled = True
        Me.cbLanguageUpdate.Location = New System.Drawing.Point(82, 36)
        Me.cbLanguageUpdate.Name = "cbLanguageUpdate"
        Me.cbLanguageUpdate.Size = New System.Drawing.Size(163, 21)
        Me.cbLanguageUpdate.TabIndex = 10
        '
        'cbSelectWord
        '
        Me.cbSelectWord.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSelectWord.FormattingEnabled = True
        Me.cbSelectWord.Location = New System.Drawing.Point(8, 9)
        Me.cbSelectWord.Name = "cbSelectWord"
        Me.cbSelectWord.Size = New System.Drawing.Size(237, 21)
        Me.cbSelectWord.TabIndex = 8
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.cbRequirePassword)
        Me.TabPage2.Controls.Add(Me.btnExportPass)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.btnExport)
        Me.TabPage2.Controls.Add(Me.btnImport)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(255, 145)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Settings"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnImport
        '
        Me.btnImport.Location = New System.Drawing.Point(7, 14)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(115, 25)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "Import"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Location = New System.Drawing.Point(128, 14)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(118, 25)
        Me.btnExport.TabIndex = 1
        Me.btnExport.Text = "Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(110, 89)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(136, 20)
        Me.TextBox1.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Export password:"
        '
        'btnExportPass
        '
        Me.btnExportPass.Location = New System.Drawing.Point(9, 58)
        Me.btnExportPass.Name = "btnExportPass"
        Me.btnExportPass.Size = New System.Drawing.Size(237, 25)
        Me.btnExportPass.TabIndex = 4
        Me.btnExportPass.Text = "Export with password"
        Me.btnExportPass.UseVisualStyleBackColor = True
        '
        'cbRequirePassword
        '
        Me.cbRequirePassword.AutoSize = True
        Me.cbRequirePassword.Location = New System.Drawing.Point(19, 117)
        Me.cbRequirePassword.Name = "cbRequirePassword"
        Me.cbRequirePassword.Size = New System.Drawing.Size(187, 17)
        Me.cbRequirePassword.TabIndex = 6
        Me.cbRequirePassword.Text = "Require password on every action"
        Me.cbRequirePassword.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(8, 89)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(53, 21)
        Me.btnDelete.TabIndex = 16
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'lblStatusUpdate
        '
        Me.lblStatusUpdate.Location = New System.Drawing.Point(8, 117)
        Me.lblStatusUpdate.Name = "lblStatusUpdate"
        Me.lblStatusUpdate.Size = New System.Drawing.Size(237, 20)
        Me.lblStatusUpdate.TabIndex = 17
        Me.lblStatusUpdate.Text = "ERR"
        Me.lblStatusUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FormTranslate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(261, 168)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormTranslate"
        Me.Text = "Translate"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents lblStatus As Label
    Friend WithEvents btnCheck As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtTranslation As TextBox
    Friend WithEvents cbLanguage As ComboBox
    Friend WithEvents cbSourceWord As ComboBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents lblTotalWords As Label
    Friend WithEvents btnWordAdd As Button
    Friend WithEvents txtWordAdd As TextBox
    Friend WithEvents btnUpdate As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents txtWordUpdate As TextBox
    Friend WithEvents cbLanguageUpdate As ComboBox
    Friend WithEvents cbSelectWord As ComboBox
    Friend WithEvents lblStatusAdded As Label
    Friend WithEvents cbRequirePassword As CheckBox
    Friend WithEvents btnExportPass As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents btnExport As Button
    Friend WithEvents btnImport As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents lblStatusUpdate As Label
End Class
