﻿Imports System.ComponentModel
Imports TranslateStrings.MultiLanguage

Partial Public Class FormTranslate ' Add Word Tab
    Private Sub InitAddWordTab() Handles MyBase.Load
        Me.lblStatusAdded.ResetText()
    End Sub

    Private Sub AddWord() Handles btnWordAdd.Click
        If Me.txtWordAdd.TextLength = 0 Then
            ShowAddWordError("Word must be at least one character!")
            Return
        End If

        If Me.Dictionary.Any(Function(w) w.Value = Me.txtWordAdd.Text) Then
            Me.ShowAddWordError("Word already exists!")
            Return
        End If

        If Not Me.CorrectPasswordIfRequired() Then
            Me.ShowAddWordError("Wrong password!")
            Return
        End If

        Me.Dictionary.Add(New MultiLangStr(Me.txtWordAdd.Text))

        Me.lblStatusAdded.Text = "Word added sucessfully!"
        Me.lblStatusAdded.ForeColor = Color.Green
    End Sub

    Private Sub ShowAddWordError(message As String)
        Me.lblStatusAdded.Text = message
        Me.lblStatusAdded.ForeColor = Color.Red
    End Sub

    Private Sub UpdateTotalWords() Handles btnWordAdd.Click, btnImport.Click, MyBase.Load
        Me.lblTotalWords.Text = "Total words: " & Me.Dictionary.Count()
    End Sub
End Class