﻿Imports TranslateStrings.MultiLanguage

Public Class FormTranslate ' Translate Tab
    Private Sub InitTranslateTab() Handles MyBase.Load
        Me.cbSourceWord.DataSource = Dictionary
        Me.cbLanguage.DataSource = Languages
        Me.lblStatus.Text = String.Empty
    End Sub

    Private Sub CheckTranslation() Handles btnCheck.Click
        Dim selectedMStr = DirectCast(Me.cbSourceWord.SelectedItem, MultiLangStr)

        If selectedMStr.IsCorrectTranslation(Me.txtTranslation.Text, cbLanguage.SelectedIndex) Then
            Me.lblStatus.Text = "Correct!"
            Me.lblStatus.ForeColor = Color.Green
        Else
            Me.lblStatus.Text = "Wrong!"
            Me.lblStatus.ForeColor = Color.Red
        End If
    End Sub
End Class
