﻿Imports TranslateStrings.MultiLanguage

Partial Public Class FormTranslate ' Update Word Tab
    Private selectedMStr As MultiLangStr

    Private Sub InitUpdateWordTab() Handles MyBase.Load
        Me.cbSelectWord.DataSource = Dictionary
        Me.cbLanguageUpdate.DataSource = Languages
        lblStatusUpdate.ResetText()
    End Sub

    Private Sub UpdateWordTranslation() Handles btnUpdate.Click
        If Me.txtWordUpdate.TextLength = 0 Then
            ShowUpdateError("Word must be at least one character!")
            Return
        End If

        If Not Me.CorrectPasswordIfRequired() Then
            Me.ShowUpdateError("Wrong password!")
            Return
        End If

        Me.selectedMStr.Translations(cbLanguageUpdate.SelectedIndex).Value = Me.txtWordUpdate.Text
        Me.ShowUpdateSuccess("Updated translation sucessfully!")
    End Sub

    Private Sub DeleteWord() Handles btnDelete.Click
        If Not Me.CorrectPasswordIfRequired() Then
            Me.ShowUpdateError("Wrong password!")
            Return
        End If

        Me.Dictionary.Remove(Me.cbSelectWord.SelectedItem)
        Me.ShowUpdateSuccess("Deleted word sucessfully!")
    End Sub

    Private Sub ShowUpdateSuccess(message As String)
        lblStatusUpdate.Text = message
        lblStatusUpdate.ForeColor = Color.Green
    End Sub

    Private Sub ShowUpdateError(message As String)
        lblStatusUpdate.Text = message
        lblStatusUpdate.ForeColor = Color.Red
    End Sub

    Private Sub UpdateSelectedMStr() Handles cbSelectWord.SelectedValueChanged, MyBase.Load
        Me.selectedMStr = DirectCast(cbSelectWord.SelectedItem, MultiLangStr)
    End Sub

    Private Sub UpdateWordUpdateTxt() Handles cbSelectWord.SelectedValueChanged, cbLanguageUpdate.SelectedValueChanged, MyBase.Load
        txtWordUpdate.Text = If(Me.selectedMStr.Translations(cbLanguageUpdate.SelectedIndex).Value, String.Empty)
    End Sub
End Class
