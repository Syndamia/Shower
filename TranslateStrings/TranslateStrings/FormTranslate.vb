﻿Imports System.ComponentModel
Imports System.Security.Cryptography
Imports System.Text.Encoding
Imports TranslateStrings.MultiLanguage

Public Class FormTranslate
    Private Dictionary As BindingList(Of MultiLangStr) = New BindingList(Of MultiLangStr) From {
        New MultiLangStr("Red"),
        New MultiLangStr("Green"),
        New MultiLangStr("Blue")
    } ' Default values
    Private Languages As BindingList(Of String) = New BindingList(Of String)([Enum].GetNames(GetType(Language)))

    Private PasswordPrompt As PasswordPrompt = New PasswordPrompt()
    Private RequirePassword As Boolean = False
    Private PasswordHASH As String = String.Empty

    Public Function HashString(value As String) As String
        Static Dim hashAlg As SHA256 = SHA256.Create()
        Dim hash As Byte() = hashAlg.ComputeHash(UTF8.GetBytes(value))
        Return BitConverter.ToString(hash).Replace("-", String.Empty)
    End Function

    Public Function CorrectPasswordIfRequired() As Boolean
        If Me.RequirePassword Then
            Me.PasswordPrompt.ShowDialog()
            Dim InputPassword As String = Me.PasswordPrompt.txtPassword.Text

            If Me.PasswordHASH <> Me.HashString(InputPassword) Then
                Return False
            End If
        End If
        Return True
    End Function
End Class