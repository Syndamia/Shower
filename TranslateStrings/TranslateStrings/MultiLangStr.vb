﻿Namespace MultiLanguage
    Public Class MultiLangStr
        Public ReadOnly Value As String
        Public ReadOnly Translations() As LangStr

        Public Sub New(value As String)
            Me.Value = value

            Dim temp = New HashSet(Of LangStr)
            For Each lang As Integer In [Enum].GetValues(GetType(Language))
                temp.Add(New LangStr(String.Empty, lang))
            Next
            Me.Translations = temp.ToArray()
        End Sub

        Public Function IsCorrectTranslation(translation As String, language As Language)
            Return Normalize(translation) =
                   Normalize(Translations.First(Function(t) t.ValueLanguage = language).Value)
        End Function

        Private Function Normalize(value As String) As String
            Return value.Replace(" ", String.Empty).ToUpperInvariant()
        End Function

        Public Overrides Function ToString() As String
            Return Value
        End Function
    End Class
End Namespace