﻿Namespace MultiLanguage
    Public Class LangStr
        Public Value As String
        Public ReadOnly ValueLanguage As Language

        Public Sub New(value As String, valueLanguage As Language)
            Me.Value = value
            Me.ValueLanguage = valueLanguage
        End Sub

        Public Overrides Function ToString() As String
            Return Value
        End Function
    End Class
End Namespace