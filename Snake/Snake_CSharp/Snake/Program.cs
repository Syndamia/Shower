﻿using System;
using System.Linq;
using System.Threading;

namespace Snake {
    class MainClass {
        private const int DEFAULT_MAP_HEIGHT = 20;
        private const int DEFAULT_MAP_WIDTH = 20;

        private const string FIRST_DIRECTION = "right";

        private static ConsoleKey moveUp = ConsoleKey.W;
        private static ConsoleKey moveDown = ConsoleKey.S;
        private static ConsoleKey moveRight = ConsoleKey.D;
        private static ConsoleKey moveLeft = ConsoleKey.A;

        public static void Main(string[] args) {
            InitialSetup();
            bool playing = true;

            string direction = FIRST_DIRECTION;
            while (playing) {
                if (Console.KeyAvailable) {
                    ConsoleKey pressedKey = Console.ReadKey().Key;

                    if (pressedKey == moveUp) direction = "up";
                    else if (pressedKey == moveDown) direction = "down";
                    else if (pressedKey == moveRight) direction = "right";
                    else if (pressedKey == moveLeft) direction = "left";
                }
                Snake.MoveHead(direction);
                Food.IfEaten();
                PrintMap();
                if (Snake.HasBittenItself) playing = false;
                else {
                    Thread.Sleep(200);
                    Console.Clear();
                }
            }
            Console.WriteLine("YOU LOST!");
        }

        public static void PrintMap() {
            for(int row = 0; row <= Map.Height + 1; row++) {
                Console.WriteLine(Map.GetLine(row));
            }
        }

        public static void InitialSetup() {
            Map.ChangeSize(DEFAULT_MAP_HEIGHT, DEFAULT_MAP_WIDTH);
            Snake.AddBodyPart(2, 1, '+');
            Snake.SetInitialHeadDirection(FIRST_DIRECTION);
            Food.GenerateRandomPosition();
        }
    }
}
