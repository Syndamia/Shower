﻿using System;
using System.Text;

namespace Snake {
    public static class Map {
        //Height and width are the playable area and there is a frame around 
        public static int Height { get; private set; }
        public static int Width { get; private set; }

        public static string GetLine(int line) {
            if (line == 0 || line == Height + 1) return new string('-', Width + 2);

            StringBuilder toReturn = new StringBuilder("|");

            char toAppend = ' ';
            for(int x = 1; x <= Width; x++) {
                if (Snake.AnyBodyPartIsAtPosition( x, line )) toAppend = Snake.GetBodySymbol( x, line );
                else if (Food.IsAtPosition( x, line )) toAppend = Food.Symbol;
                toReturn.Append(toAppend);
                toAppend = ' ';
            }
            return toReturn.Append("|").ToString();
        }

        public static void ChangeSize(int height, int width) {
            Map.Height = height;
            Map.Width = width;
        }
    }
}
