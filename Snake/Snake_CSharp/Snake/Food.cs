﻿using System;
namespace Snake {
    public static class Food {
        public static int XPos { get; private set; }
        public static int YPos { get; private set; }
        public static char Symbol = 'O';

        private static Random rnd = new Random();

        public static void IfEaten() {
            if (Snake.AnyBodyPartIsAtPosition(XPos, YPos )) {
                GenerateRandomPosition();
                Snake.AppendBodyPart();
            }
        }

        public static void GenerateRandomPosition() {
            XPos = rnd.Next(1, Map.Width + 1);
            YPos = rnd.Next(1, Map.Height + 1);
        }

        public static bool IsAtPosition(int XPos, int YPos) {
            return Food.XPos == XPos && Food.YPos == YPos;
        }
    }
}
