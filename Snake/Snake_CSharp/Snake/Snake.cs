﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Snake {
    public static class Snake {
        private const char DEFAULT_BODY_SYMBOL = '*';
        private static List<BodyPart> body = new List<BodyPart>();

        public static void AddBodyPart(int XPos, int YPos) {
            AddBodyPart(XPos, YPos, DEFAULT_BODY_SYMBOL);
        }

        public static void AddBodyPart(int XPos, int YPos, char Symbol) {
            Body.Add(new BodyPart(XPos, YPos, Symbol));
        }

        public static void MoveHead(string direction) {
            for(int i = Body.Count() - 1; i > 0; i--) {
                Body[i].Move(Body[i - 1].LastDirection);
            }
            Body[0].Move(direction);
        }

        public static void AppendBodyPart() {
            int currXPos = Body.Last().XPos, currYPos = Body.Last().YPos;

            switch (Body.Last().LastDirection) {
                case "up":
                    if (currYPos == Map.Height) currYPos = 1; 
                    else currYPos++; 
                    break;
                case "down":
                    if (currYPos == 1) currYPos = Map.Height;
                    else currYPos--;
                    break;
                case "left":
                    if (currXPos == Map.Width) currXPos = 1;
                    else currXPos++;
                    break;
                case "right":
                    if (currXPos == 1) currXPos = Map.Width;
                    else currXPos--;
                    break;
            }
            Snake.Body.Add(new BodyPart(currXPos, currYPos, '*'));
        }

        public static bool HasBittenItself {
            get => Body.Count(bp => bp.XPos == Body.First().XPos && bp.YPos == Body.First().YPos) > 1;
        }

        public static bool AnyBodyPartIsAtPosition(int XPos, int YPos) {
            return Body.Any(bp => bp.IsOnPosition(XPos, YPos));
        }

        public static char GetBodySymbol(int XPos, int YPos) {
            return Body.First( bp => bp.IsOnPosition( XPos, YPos ) ).Symbol;
        }

        public static void SetInitialHeadDirection(string direction) {
            Body.First().UpdateLastDirection(direction);
        }

        public static List<BodyPart> Body {
            get => Snake.body;
        }
    }
}
