﻿using System;
namespace Snake {
    public class BodyPart {
        public int XPos { get; private set; }
        public int YPos { get; private set; }
        public string LastDirection { get; private set; }

        public char Symbol { get; set; }

        public BodyPart(int XPos, int YPos, char Symbol) {
            this.XPos = XPos;
            this.YPos = YPos;
            this.Symbol = Symbol;
        }

        public bool IsOnPosition(int XPos, int YPos) {
            return XPos == this.XPos && YPos == this.YPos;
        }

        public void Move(string direction) {
            switch (direction) {
                case "up":
                    if (YPos == 1) YPos = Map.Height;
                    else YPos--;
                    break;
                case "down":
                    if (YPos == Map.Height) YPos = 1;
                    else YPos++;
                    break;
                case "left":
                    if (XPos == 1) XPos = Map.Width;
                    else XPos--;
                    break;
                case "right":
                    if (XPos == Map.Width) XPos = 1;
                    else XPos++;
                    break;
            }
            UpdateLastDirection(direction);
        }

        public void UpdatePosition(int newXPos, int newYPos) {
            this.XPos = newXPos;
            this.YPos = newYPos;
        }

        //this exists because of Snake.SetInitialHeadDirection
        public void UpdateLastDirection(string direction) {
            LastDirection = direction;
        }
    }
}
