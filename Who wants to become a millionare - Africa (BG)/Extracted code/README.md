This is the code that operates the whole presentation. I have extracted 4 slides.

 - Slide 2: It contains methods that operate on all slides and information like correct answers. It serves as a connection between everything.
 - Slide 3: It contains the main code that is used by most slides (slide 3 to 17). It changes button colors, button avalability, has logic for lifelines, etc. It has the code that does the hevy lifting. Slides 3 to 17 have the same code with certain changed values.
 - Slide 17: This is the slide with the last question. The only difference to the other slides with questions is when you win (rather than going to next slide it ends the game; Line 96)
 - Slide 18: This is the last slide of the presentation. This is where you end up after ending a game (loosing, keeping money or winning). It is nothing special, just descided to include it because it is different.