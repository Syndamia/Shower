VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Slide17"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Dim correctAns As Boolean
Const currQuestion As Integer = 15

'Button_Click events

Private Sub cmdAnswerA_Click()

If Not (lblSureAbtAns.Visible) Then
    Mark "A"
Else
    Finalize "A"
End If

End Sub

Private Sub cmdAnswerB_Click()

If Not (lblSureAbtAns.Visible) Then
    Mark "B"
Else
    Finalize "B"
End If

End Sub

Private Sub cmdAnswerC_Click()

If Not (lblSureAbtAns.Visible) Then
    Mark "C"
Else
    Finalize "C"
End If

End Sub

Private Sub cmdAnswerD_Click()

If Not (lblSureAbtAns.Visible) Then
    Mark "D"
Else
    Finalize "D"
End If

End Sub

Private Sub cmdFiftyFifty_Click()

Slide2.UseFiftyFifty

Dim keepWrong As String, currCorrect As String
keepWrong = Slide2.GetWrongAnsToKeep(currQuestion)
currCorrect = Slide2.GetCorrAnswer(currQuestion)

VisibleAnswer "All", False
VisibleAnswer keepWrong, True
VisibleAnswer currCorrect, True

End Sub


Private Sub cmdGiveUp_Click()

Slide2.EndGame 500000

End Sub

Private Sub cmdPhoneFriend_Click()

Slide2.UsePhoneFriend
imgFriendMsg.Visible = False

End Sub

Private Sub cmdAskAudience_Click()

Slide2.UseAskAudience
imgAskAudience.Visible = False

End Sub

Private Sub cmdContinue_Click()
   
If Not (correctAns) Then
Slide2.EndGame 50000
Else
Slide2.EndGame 1000000
End If
   
End Sub

' Chosing answers

Private Sub Mark(chosenAnswer As String)

ColorAnswer chosenAnswer, "Orange"
DisabledAllLifeLines True
cmdGiveUp.Enabled = False

lblSureAbtAns.Visible = True

End Sub

Private Sub Finalize(chosenAnswer As String)

If cmdAnswerA.BackColor <> 0 Or cmdAnswerB.BackColor <> 0 Or cmdAnswerC.BackColor <> 0 Or cmdAnswerD.BackColor <> 0 Then
    ColorAllAnswers "Black"
    ColorAnswer chosenAnswer, "Orange"
End If
ColorAnswer Slide2.GetCorrAnswer(currQuestion), "Green"

EnableAnswer "All", False
lblSureAbtAns.Visible = False

If chosenAnswer = Slide2.GetCorrAnswer(currQuestion) Then
    correctAns = True
End If

cmdContinue.Visible = True
    'reloads current slide, continue button can't be pressed sometimes (bug) when made visible
Application.SlideShowWindows(1).View.GotoSlide Me.SlideIndex
End Sub

'Changing answer properties

Private Sub ColorAnswer(answer As String, colorName As String)

Select Case answer
    Case "A": cmdAnswerA.BackColor = Slide2.GetColor(colorName)
    Case "B": cmdAnswerB.BackColor = Slide2.GetColor(colorName)
    Case "C": cmdAnswerC.BackColor = Slide2.GetColor(colorName)
    Case "D": cmdAnswerD.BackColor = Slide2.GetColor(colorName)
End Select

End Sub

Private Sub ColorAllAnswers(colorName As String)

ColorAnswer "A", colorName
ColorAnswer "B", colorName
ColorAnswer "C", colorName
ColorAnswer "D", colorName

End Sub

Private Sub EnableAnswer(answer As String, isEnabled As Boolean)

Select Case answer
    Case "A": cmdAnswerA.Enabled = isEnabled
    Case "B": cmdAnswerB.Enabled = isEnabled
    Case "C": cmdAnswerC.Enabled = isEnabled
    Case "D": cmdAnswerD.Enabled = isEnabled
    Case "All":
        EnableAnswer "A", isEnabled
        EnableAnswer "B", isEnabled
        EnableAnswer "C", isEnabled
        EnableAnswer "D", isEnabled
End Select

End Sub

Private Sub VisibleAnswer(answer As String, isVisible As Boolean)

Select Case answer
    Case "A": cmdAnswerA.Visible = isVisible
    Case "B": cmdAnswerB.Visible = isVisible
    Case "C": cmdAnswerC.Visible = isVisible
    Case "D": cmdAnswerD.Visible = isVisible
    Case "All":
        VisibleAnswer "A", isVisible
        VisibleAnswer "B", isVisible
        VisibleAnswer "C", isVisible
        VisibleAnswer "D", isVisible
End Select

End Sub

'Public methods

Public Sub DisabledLifeLine(lifeLineName As String, isDisabled As Boolean)

Select Case lifeLineName
    Case "FiftyFifty": cmdFiftyFifty.Enabled = Not (isDisabled)
    Case "PhoneFriend": cmdPhoneFriend.Enabled = Not (isDisabled)
    Case "AskAudience": cmdAskAudience.Enabled = Not (isDisabled)
End Select

End Sub

Public Sub DisabledAllLifeLines(areDisabled As Boolean)

DisabledLifeLine "FiftyFifty", areDisabled
DisabledLifeLine "PhoneFriend", areDisabled
DisabledLifeLine "AskAudience", areDisabled

End Sub

Public Sub Reset()

correctAns = False

DisabledAllLifeLines False
cmdGiveUp.Enabled = True
EnableAnswer "All", True
VisibleAnswer "All", True

ColorAllAnswers "Black"

cmdContinue.Visible = False
imgFriendMsg.Visible = True
imgAskAudience.Visible = True

End Sub

Public Property Get GuessedCorrectly()
    GuessedCorrectly = correctAns
End Property

'Kamen Mladenov 2019

Private Sub imgFriendMsg_Click()

End Sub
