namespace ARK.exemodule
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("ARK.exemodule");
           
            Console.WriteLine("");
           


            string Command = Console.ReadLine();
            {
                if (Command.ToLower() == "/scan")
                {
                    Console.WriteLine(">ARK memory = 1231 TB");
                    Console.WriteLine(">Starting memory scan...");

                    Random randomObject = new Random();

                    int numberOfSecondsToSleep = randomObject.Next(5) + 1;
                    Thread.Sleep(numberOfSecondsToSleep * 1000);

                   
                    Random randomObject4 = new Random();
                    int numberOfSecondsToSleep4 = randomObject4.Next(5) + 3;
                    Thread.Sleep(numberOfSecondsToSleep4 * 1000);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("");
                    Console.WriteLine("             SYSTEMS");
                    Console.WriteLine(" sys_physics_rigid    [ 28 TB]");
                    Console.WriteLine(" sys_physics_soft     [178 TB]");
                    Console.WriteLine(" sys_physics_fluid    [201 TB]");
                    Console.WriteLine(" sys_physics_buoy     [ 40 TB]");
                    Console.WriteLine(" sys_wmod_atmo        [ 51 TB]");
                    Console.WriteLine(" sys_interface_scan   [ 43 TB]");
                    Console.WriteLine(" sys_wmod_heat        [ 78 TB]");
                    Console.WriteLine(" sys_wmoddynlight     [ 46 TB]");
                    Console.WriteLine(" sys_gen_veg_basic    [ 17 TB]");
                    Console.WriteLine(" sys_gen_veg_abv      [ 24 TB]");

                    Random randomObject60 = new Random();
                    int numberOfSecondsToSleep60 = randomObject.Next(3) + 1;
                    Thread.Sleep(numberOfSecondsToSleep * 1000); 

                    Console.WriteLine("");
                    Console.WriteLine("             SCENES");
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine(" scn_plains_grass     [230 TB]");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine(" scn_forest           [259 TB]");

                    Random randomObject61 = new Random();
                    int numberOfSecondsToSleep61 = randomObject.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep * 1000); 

                    Console.WriteLine("");
                    Console.WriteLine("            OBJECTS");
                    Console.WriteLine(" pkg_scan_human=14    [130 TB]");
                    Console.WriteLine(" stc_env_dirt_road    [ 86 TB]");
                    Console.WriteLine(" ent_pk_1_bench_v2    [ 20 TB]");
                    Console.WriteLine(" ent_pk_boat          [132 TB]");
                    Console.WriteLine(" env_mod_river        [172 TB]");
                    Console.WriteLine(" ent_pj_1_chair       [ 18 TB]");
                    Console.WriteLine(" stc_env_cabin_ex     [172 TB]");
                    Console.WriteLine(" env_mod_lake         [165 TB]");
                    Console.WriteLine(" ent_foliage_sml      [ 69 TB]");
                    Console.WriteLine(" ext_sun_noon         [154 TB]");
                    Console.WriteLine(" ext_moon_full        [ 66 TB]");
                    Console.WriteLine(" ext_clouds_dy        [ 82 TB]");

                    Console.WriteLine("");
                    Console.WriteLine(">Loading files     ");

                    Random randomObject3 = new Random();
                    int numberOfSecondsToSleep3 = randomObject3.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep3 * 1000);
                    Console.Write(" .");

                    Random randomObject1 = new Random();
                    int numberOfSecondsToSleep1 = randomObject1.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep1 * 1000);

                    Console.Write(".");

                    Random randomObject5555555 = new Random();
                    int numberOfSecondsToSleep5555555 = randomObject1.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep1 * 1000);

                    Console.Write(".");
                    Random randomObject2 = new Random();
                    int numberOfSecondsToSleep2 = randomObject1.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep1 * 1000);

                    Console.Write(".");
                    Random randomObject100 = new Random();
                    int numberOfSecondsToSleep100 = randomObject1.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep1 * 1000);

                    Console.Write(".");
                    Random randomObject101 = new Random();
                    int numberOfSecondsToSleep101 = randomObject1.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep1 * 1000);

                    Console.WriteLine("");
                    
                    Console.WriteLine(">Files loaded");
                     
                }

                {
                Console.WriteLine(">Activating ARK for booting up...");

                    Random randomObject3 = new Random();

                    int numberOfSecondsToSleep3 = randomObject3.Next(4) + 1;
                    Thread.Sleep(numberOfSecondsToSleep3 * 1000);

                    Console.WriteLine(">Loading SCENE.module = scn_forest ...");
                    Console.WriteLine("");
                    Console.WriteLine(" SCENE.module is ready");
                    Console.WriteLine("");
                    Console.WriteLine(">Loading SYSTEM.module ... ");

                    
                    Random randomObject4 = new Random();
                    int numberOfSecondsToSleep4 = randomObject4.Next(5) + 3;
                    Thread.Sleep(numberOfSecondsToSleep4 * 1000);

                    Console.WriteLine(">Prepairing systems....");
                    Console.WriteLine("");
                    Console.WriteLine(" sys_physics_rigid = activated");
                   

                    Random randomObject6 = new Random();
                    int numberOfSecondsToSleep6 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_physics_soft = activated");

                    Random randomObject31 = new Random();
                    int numberOfSecondsToSleep31 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_physics_fluid = activated");

                    Random randomObject32 = new Random();
                    int numberOfSecondsToSleep32 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_phys_buoy = activated");

                    Random randomObject33 = new Random();
                    int numberOfSecondsToSleep33 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_wmod_atmo = activated");

                    Random randomObject34 = new Random();
                    int numberOfSecondsToSleep34 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_interface_scan = activated");
                    
                    Random randomObject35 = new Random();
                    int numberOfSecondsToSleep35 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_wmod_heat = activated");

                    Random randomObject7 = new Random();
                    int numberOfSecondsToSleep7 = randomObject7.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep7 * 1000);

                    Console.WriteLine(" sys_wmod_dynlight = activated");

                    Random randomObject36 = new Random();
                    int numberOfSecondsToSleep36 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_gen_veg_basic = activated");

                    Random randomObject37 = new Random();
                    int numberOfSecondsToSleep37 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" sys_gen_veg_abv = activated");
                    Console.WriteLine("");
                    Console.WriteLine(" SYSTEM.module is ready");
                    Console.WriteLine("");
                    Console.WriteLine(">Loading OBJECTS.modules ...");

                    Random randomObject38 = new Random();
                    int numberOfSecondsToSleep38 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine("");
                    Console.WriteLine(">Prepairing objects ...");
                    
                    Random randomObject9 = new Random();
                    int numberOfSecondsToSleep9 = randomObject9.Next(4) + 1;
                    Thread.Sleep(numberOfSecondsToSleep9 * 1000);
                    
                    Console.WriteLine(" pkg_scan_hum=14 = activated");

                    Random randomObject39 = new Random();
                    int numberOfSecondsToSleep39 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" stc_env_dirt_road = activated");

                    Random randomObject40 = new Random();
                    int numberOfSecondsToSlee40 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine(" ent_pk_1_bench_v2 = activated");
           
                    Random randomObject41 = new Random();
                    int numberOfSecondsToSlee41 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" ent_pk_boat = activated");

                    Random randomObject42 = new Random();
                    int numberOfSecondsToSlee42 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" env_mod_river = activated");
                    Console.WriteLine(" ent_pj_1_chair = activated");

                    Random randomObject8 = new Random();
                    int numberOfSecondsToSleep8 = randomObject8.Next(5) + 3;
                    Thread.Sleep(numberOfSecondsToSleep8 * 1000);
                   


                    Random randomObject43 = new Random();
                    int numberOfSecondsToSlee43 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);
                    
                    Console.WriteLine(" stc_env_cabin_ex = activated");

                    Random randomObject44 = new Random();
                    int numberOfSecondsToSlee44 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" env_mod_lake = activated");

                    Random randomObject45 = new Random();
                    int numberOfSecondsToSlee45 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" ent_foliage_sml = activated");

                    Random randomObject46 = new Random();
                    int numberOfSecondsToSlee46 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" ext_sun_noon = activated");

                    Random randomObject47 = new Random();
                    int numberOfSecondsToSlee47 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine(" ext_moon_full = activated");

                    Random randomObject48 = new Random();
                    int numberOfSecondsToSlee48 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);
                 
                    Console.WriteLine(" ext_clouds_dy = activated ");

                    Random randomObject49 = new Random();
                    int numberOfSecondsToSlee49 = randomObject6.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep6 * 1000);

                    Console.WriteLine("");
                    Console.WriteLine(" OBJECTS.module is ready");
                    Console.WriteLine("");
                    Console.WriteLine(">ARK status = ready");
                    Console.WriteLine(" Type /start to boot up the ARK");

              }
            string Start = Console.ReadLine();
                if (Start.ToLower() == "/start")
                {
                    Console.WriteLine(">Booting up ...");

                    Random randomObject10 = new Random();
                    int numberOfSecondsToSleep10 = randomObject10.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine(" Booting up systems ...");
                    
                    Random randomObject11 = new Random();
                    int numberOfSecondsToSleep11 = randomObject11.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep11 * 1000);
                   
                    Console.WriteLine("");
                    Console.WriteLine(">Activating SYSTEMS.module...");
                    
                    Random randomObject12 = new Random();
                    int numberOfSecondsToSleep12 = randomObject12.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep12 * 1000);
                    
                    Console.WriteLine(" SYSTEMS.module is active");
                    Console.WriteLine("");
                    Console.WriteLine(">Activating SCENES.module...");
                    
                    Random randomObject13 = new Random();
                    int numberOfSecondsToSleep13 = randomObject13.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep13 * 1000);
                    
                    Console.WriteLine(" SCENES.module is active");
                    Console.WriteLine("");
                    Console.WriteLine(">Activating OBJECTS.module...");

                    Random randomObject14 = new Random();
                    int numberOfSecondsToSleep14 = randomObject14.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep14 * 1000);
                    
                    Console.WriteLine(" OBJECTS.module is active ");
                    Console.WriteLine("");
                    Console.WriteLine(">ARK activated...");

                    Random randomObject15 = new Random();
                    int numberOfSecondsToSleep15 = randomObject15.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep15 * 1000);
                    
                    Console.WriteLine(" starting all systems...");

                    Random randomObject16 = new Random();
                    int numberOfSecondsToSleep16 = randomObject16.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep16 * 1000);
                    
                    Console.WriteLine(">All systems started");
                    Console.WriteLine(">Connecting modules...");
                  
                    Random randomObject67 = new Random();
                    int numberOfSecondsToSleep67 = randomObject10.Next(3) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);
                    Console.WriteLine(" Activating all hum_scan 's...");
                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.White;

                    
                    Console.WriteLine("System> hum_scan_Simon_Jarrett=active");

                    Random randomObject17 = new Random();
                    int numberOfSecondsToSleep17 = randomObject17.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep17 * 1000);

                    Console.WriteLine("System> hum_scan_Simon_Jarrett=talking.movement[Whoa.Is this? Did it work?]");
                    Console.WriteLine("");
                    Console.WriteLine("System> hum_scan_Simon_Jarrett=moving.folowing_plath");
                    Console.WriteLine("");

                    Random randomObject18 = new Random();
                    int numberOfSecondsToSleep18 = randomObject18.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep18 * 1000);
              

                    Random randomObject19 = new Random();
                    int numberOfSecondsToSleep19 = randomObject19.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep19 * 1000);

                    Console.WriteLine("System> hum_scan_Cathrene_Chun=active");
                    Console.WriteLine("");


                    Random randomObject50 = new Random();
                    int numberOfSecondsToSleep50 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Mark_Stronmeier=active");
                    Console.WriteLine("");

                    Random randomObject51 = new Random();
                    int numberOfSecondsToSleep51 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Guy_Konrad=active");
                    Console.WriteLine("");

                    Random randomObject53 = new Random();
                    int numberOfSecondsToSleep53 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Madine_Masters=active");
                    Console.WriteLine("");

                     Random randomObject54 = new Random();
                    int numberOfSecondsToSleep54 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Brandon_Wan=active");
                    Console.WriteLine("");

                    Random randomObject55 = new Random();
                    int numberOfSecondsToSleep55 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Javid_Goya=active");
                    Console.WriteLine("");

                    Random randomObject56 = new Random();
                    int numberOfSecondsToSleep56 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_John_Stronmeier=active");
                    Console.WriteLine("");

                    Random randomObject57 = new Random();
                    int numberOfSecondsToSleep57 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Robin_Bass=active");
                    Console.WriteLine("");

                    Random randomObject58 = new Random();
                    int numberOfSecondsToSleep58 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_MartinFisher=active");
                    Console.WriteLine("");

                    Random randomObject59 = new Random();
                    int numberOfSecondsToSleep59 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Peter_Strasky=active");
                    Console.WriteLine("");

                    Random randomObject60 = new Random();
                    int numberOfSecondsToSleep60 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Steve_Glasser=active");
                    Console.WriteLine("");

                    Random randomObject61 = new Random();
                    int numberOfSecondsToSleep61 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("System> hum_scan_Brandon_Wan=active");
                    Console.WriteLine("");

                    Random randomObject20 = new Random();
                    int numberOfSecondsToSleep20 = randomObject20.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep20 * 1000);

                    Console.WriteLine("System> hum_scan-Simon_Jarrett=moving.folowing_plath[finished]");
                    Console.WriteLine("");

                    Random randomObject62 = new Random();
                    int numberOfSecondsToSleep62 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000); 

                    Console.WriteLine("System> hum_scan_Simon_Jarrett=talking.movement[Cathrene?]");
                    Console.WriteLine("");

                    Random randomObject63 = new Random();
                    int numberOfSecondsToSleep63 = randomObject10.Next(1) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000); 

                    Console.WriteLine("System> hum_scan_Simon_Jarrett=talking.movement[Cathrene!]");
                    Console.WriteLine("");

                    Random randomObject64 = new Random();
                    int numberOfSecondsToSleep64 = randomObject10.Next(2) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000); 

                    Console.WriteLine("System> hum_scan_Simon_Jarrett=talking.movement[I can't believe we actualy made it.]");
                    Console.WriteLine("");

                    Random randomObject65 = new Random();
                    int numberOfSecondsToSleep65 = randomObject10.Next(1) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000); 

                    Console.WriteLine("System> hum_scan_Catherene_Chun=talking.movement[Well, we did.]");
                    Console.WriteLine("");

                    Random randomObject66 = new Random();
                    int numberOfSecondsToSleep66 = randomObject10.Next(1) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000); 

                    Console.WriteLine("System> hum_scan_Simon_Jarrett=talking.movement[I'm so relieved.]");
                    Console.WriteLine("");

                    Random randomObject68 = new Random();
                    int numberOfSecondsToSleep68 = randomObject10.Next(1) + 1;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000); 

                    Console.WriteLine("System> hum_scan_Catherene_Chun=talking.movement[It's okay Simon. Everything's all right now.]");
                    Console.WriteLine("");

                    
                }
                else
                {
                    Console.WriteLine("Stopping systems....");

                    Random randomObject10 = new Random();
                    int numberOfSecondsToSleep10 = randomObject10.Next(3) + 2;
                    Thread.Sleep(numberOfSecondsToSleep10 * 1000);

                    Console.WriteLine("Systems deactivated");
                }


                
           }

        }
    }
}
