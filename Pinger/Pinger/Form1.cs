﻿using System;
using System.Threading;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pinger {
    public partial class myNetForm : Form {
        private Thread t;

        public myNetForm() {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;

            t = new Thread(PingStatus);
            t.Start();
        }

        private void PingStatus() {
            Ping p = new Ping();
            PingReply pr;

            while(!this.IsDisposed) {
                lblNotifyPingEvent.Visible = true;
                try {
                    pr = p.Send(cbPingAddress.Text);
                    lblErrorMessage.Text = "";

                    if (pr.Status == IPStatus.Success) {
                        lblPingStatus.Text = pr.RoundtripTime + "";

                        if (pr.RoundtripTime > 300) lblPingStatus.BackColor = Color.Red;
                        else if (pr.RoundtripTime > 100) lblPingStatus.BackColor = Color.Orange;
                        else if (pr.RoundtripTime > 10) lblPingStatus.BackColor = Color.Yellow;
                        else lblPingStatus.BackColor = Color.LightGreen;
                    }
                    else throw new Exception(pr.Status.ToString());
                } catch (Exception ex) {
                    lblErrorMessage.Text = ex.Message;

                    lblPingStatus.BackColor = Color.Red;
                    lblPingStatus.Text = "-1";
                }
                Thread.Sleep(200);
                lblNotifyPingEvent.Visible = false;
                Thread.Sleep(800);
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
        }
    }
}
