﻿namespace Pinger {
    partial class myNetForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblPing = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPingStatus = new System.Windows.Forms.Label();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.cbPingAddress = new System.Windows.Forms.ComboBox();
            this.lblNotifyPingEvent = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPing
            // 
            this.lblPing.AutoSize = true;
            this.lblPing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPing.Location = new System.Drawing.Point(31, 57);
            this.lblPing.Name = "lblPing";
            this.lblPing.Size = new System.Drawing.Size(44, 20);
            this.lblPing.TabIndex = 0;
            this.lblPing.Text = "Ping:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Address to ping:";
            // 
            // lblPingStatus
            // 
            this.lblPingStatus.BackColor = System.Drawing.Color.Red;
            this.lblPingStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPingStatus.Location = new System.Drawing.Point(135, 45);
            this.lblPingStatus.Name = "lblPingStatus";
            this.lblPingStatus.Size = new System.Drawing.Size(125, 40);
            this.lblPingStatus.TabIndex = 3;
            this.lblPingStatus.Text = "-1";
            this.lblPingStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.Location = new System.Drawing.Point(32, 99);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(236, 36);
            this.lblErrorMessage.TabIndex = 4;
            this.lblErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbPingAddress
            // 
            this.cbPingAddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbPingAddress.FormattingEnabled = true;
            this.cbPingAddress.Items.AddRange(new object[] {
            "8.8.8.8",
            "8.8.4.4",
            "1.1.1.1",
            "1.0.0.1",
            "208.67.222.222",
            "208.67.220.220"});
            this.cbPingAddress.Location = new System.Drawing.Point(117, 9);
            this.cbPingAddress.Name = "cbPingAddress";
            this.cbPingAddress.Size = new System.Drawing.Size(160, 21);
            this.cbPingAddress.TabIndex = 5;
            this.cbPingAddress.Text = "8.8.8.8";
            // 
            // lblNotifyPingEvent
            // 
            this.lblNotifyPingEvent.BackColor = System.Drawing.Color.Black;
            this.lblNotifyPingEvent.Location = new System.Drawing.Point(134, 44);
            this.lblNotifyPingEvent.Name = "lblNotifyPingEvent";
            this.lblNotifyPingEvent.Size = new System.Drawing.Size(127, 42);
            this.lblNotifyPingEvent.TabIndex = 6;
            // 
            // myNetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 144);
            this.Controls.Add(this.cbPingAddress);
            this.Controls.Add(this.lblErrorMessage);
            this.Controls.Add(this.lblPingStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPing);
            this.Controls.Add(this.lblNotifyPingEvent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "myNetForm";
            this.Text = "Pinger";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPing;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPingStatus;
        private System.Windows.Forms.Label lblErrorMessage;
        private System.Windows.Forms.ComboBox cbPingAddress;
        private System.Windows.Forms.Label lblNotifyPingEvent;
    }
}

