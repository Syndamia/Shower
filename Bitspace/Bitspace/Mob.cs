﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Mob
    {
        public byte Slots { set; get; }
        public bool Alive { set; get; }
        public bool FightingWithPlayer { set; get; }

        private int health;
        private int maxHealth;

        private List<Weapon> itemsList;
        private byte selectedItem;

        private ushort armour;
        private ushort maxArmour;

        private byte lungCapacity;
        private byte maxLungCapacity;

        private string name;
        private char body;
        private ConsoleColor color;

        private int xPos;
        private int yPos;

        public void Print()
        {
            Console.SetCursorPosition(XPos, YPos);
            Console.ForegroundColor = Color;
            Console.Write(Body);
        }

        public void PrintInventory()
        {
            Slots = 12;
            if (SelectedItem > Slots) SelectedItem = Slots;

            PrintTopOrBot(true);

            PrintMid(false);
            PrintMid(true);

            PrintTopOrBot(false);
        }

        private void PrintMid(bool printingOnlyItemAmounts)
        {
            for (byte s = 1; s <= Slots; s++)
            {
                Console.Write("│");

                if (ItemsList.Count() > s - 1)
                {
                    Console.ForegroundColor = ItemsList[s - 1].Color;

                    if (printingOnlyItemAmounts)
                    {
                        if (ItemsList[s - 1].Amount < 10) Console.Write(" " + ItemsList[s - 1].Amount + " ");
                        else if (ItemsList[s - 1].Amount < 100) Console.Write(" " + ItemsList[s - 1].Amount);
                        else Console.Write(ItemsList[s - 1].Amount);
                    }
                    else Console.Write(" " + ItemsList[s - 1].SkinUp + " ");

                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                else Console.Write("   ");
            }

            Console.WriteLine("│");
        }

        private void PrintTopOrBot(bool printingOnlyTop)
        {
            char leftAngle = '┌';
            char rightAngle = '┐';

            if (!printingOnlyTop) 
            { leftAngle = '└'; rightAngle = '┘'; }

            Console.Write(leftAngle);

            for (int s = 1; s <= Slots; s++)
            {
                if (s == SelectedItem)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("***");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write("───");
                }

                Console.ForegroundColor = ConsoleColor.Gray;
                if (s != Slots) Console.Write('─');
            }

            Console.WriteLine(rightAngle);
        }

        public void AddItemToInventory(Weapon toAdd)
        {
            if (ItemsList.Count(i => i.Name == toAdd.Name) == 1) ItemsList.Single(i => i.Name == toAdd.Name).Amount++;
            else
            {
                ItemsList.Add(new Weapon(toAdd.Name, toAdd.MaxHealth, toAdd.HealthDamage, toAdd.BreakDamage, toAdd.SkinLeft, toAdd.SkinRight, toAdd.SkinUp, toAdd.SkinDown, toAdd.Color, toAdd.Placable, toAdd.WalkOver, toAdd.Amount));
            }
        }

        public string Stats()
        {
            return $"{Name} | Health: {Health} | Armour: {Armour} | Air: {new string('O', LungCapacity)}\r\nX: {XPos} | Y: {YPos}";
        }

        public int Health
        {
            set { health = value; }
            get { return health; }
        }

        public int MaxHealth
        {
            set { maxHealth = value; }
            get { return maxHealth; }
        }


        public ushort Armour
        {
            set { armour = value; }
            get { return armour; }
        }

        public ushort MaxArmour
        {
            set { maxArmour = value; }
            get { return maxArmour; }
        }


        public byte LungCapacity 
        {
            set { lungCapacity = value; }
            get { return lungCapacity; }
        }

        public byte MaxLungCapacity
        {
            set { maxLungCapacity = value; }
            get { return maxLungCapacity; }
        }


        public string Name
        {
            set { name = value; }
            get { return name; }
        }

        public char Body 
        {
            set { body = value; }
            get { return body; }
        }

        public ConsoleColor Color
        {
            set { color = value; }
            get { return color; }
        }

        public int XPos
        {
            set { xPos = value; }
            get { return xPos; }
        }

        public int YPos
        {
            set { yPos = value; }
            get { return yPos; }
        }

        public List<Weapon> ItemsList
        {
            set { itemsList = value; }
            get { return itemsList; }
        }

        public byte SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; }
        }

        public Mob(string name, char body, ConsoleColor color, ushort health) : this(name, body, color, health, 0, 0, health, 0, 0, 0, 0, true)
        { ItemsList = new List<Weapon>(); }

        public Mob(string name, char body, ConsoleColor color, ushort health, ushort armour) : this(name, body, color, health, armour, 0, health, armour, 0, 0, 0, true)
        { ItemsList = new List<Weapon>();  }

        public Mob(string name, char body, ConsoleColor color, ushort health, ushort armour, byte lungCapacity) : this(name, body, color, health, armour, lungCapacity, health, armour, lungCapacity, 0, 0, true)
        { ItemsList = new List<Weapon>();  }

        public Mob(string name, char body, ConsoleColor color, ushort health, ushort armour, byte lungCapacity, int xPos, int yPos) : this(name, body, color, health, armour, lungCapacity, health, armour, lungCapacity, xPos, yPos, true)
        { ItemsList = new List<Weapon>();  }

        public Mob(string name, char body, ConsoleColor color, ushort health, ushort armour, byte lungCapacity, ushort maxHealth, ushort maxArmour, byte maxLungCapacity) : this(name, body, color, health, armour, lungCapacity, maxHealth, maxArmour, maxLungCapacity, 0, 0, true)
        { ItemsList = new List<Weapon>();  }

        public Mob(string name, char body, ConsoleColor color, ushort health, ushort armour, byte lungCapacity, ushort maxHealth, ushort maxArmour, byte maxLungCapacity, int xPos, int yPos) : this(name, body, color, health, armour, lungCapacity, maxHealth, maxArmour, maxLungCapacity, xPos, yPos, true)
        { ItemsList = new List<Weapon>();  }

        public Mob(string name, char body, ConsoleColor color, ushort health, ushort armour, byte lungCapacity, ushort maxHealth, ushort maxArmour, byte maxLungCapacity, int xPos, int yPos, bool alive)
        {
            Alive = alive;
            Health = health;
            MaxHealth = maxHealth;
            Armour = armour;
            MaxArmour = maxArmour;
            LungCapacity = lungCapacity;
            MaxLungCapacity = maxLungCapacity;
            Name = name;
            Body = body;
            Color = color;
            XPos = xPos;
            YPos = yPos;

            ItemsList = new List<Weapon>();
        }
    }
}
