﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game
{
    class Program
    {
        private static string[] map;
        private static StringBuilder[] objectMap;
        private static List<Weapon> objectMapObjectsInfo;

        private static Dictionary<string, Biome> biomeDict = new Dictionary<string, Biome>
        {
            { "Water", new Biome("Water", '-', ConsoleColor.Blue, 1, true) },
            { "Lava", new Biome("Lava", '#', ConsoleColor.Red, 2) },
            { "Grass land", new Biome("Grass land", ',', ConsoleColor.Green) },
            { "Rocks", new Biome("Rocks", '.', ConsoleColor.Gray) }
        };

        private static Dictionary<string, Mob> mobDict = new Dictionary<string, Mob>
        {
            { "Player", new Mob("Player", '@', ConsoleColor.White, 100, 10, 10) },
            { "Smiley", new Mob("Smiley", 'U', ConsoleColor.Magenta, 50, 5, 5) },
            { "Smiley1", new Mob("Smiley1", 'U', ConsoleColor.Magenta, 50, 5, 5) },
            { "Smiley2", new Mob("Smiley1", 'U', ConsoleColor.Magenta, 50, 5, 5) }
        };

        private static Dictionary<string, Weapon> itemDict = new Dictionary<string, Weapon>()
        {
            { "Stick", new Weapon("Stick", 3, 5, 1, '-', '|', false, false, 1) },
            { "Wall", new Weapon("Wall", 10, 3, 0, 'i', ConsoleColor.Yellow, true, false, 120) },
            { "Bridge", new Weapon("Bridge", 2, 1, 0, 'H', ConsoleColor.Cyan, true, true, 120) }
        };

        private static Dictionary<string, ConsoleKey> keyBindingsDict = new Dictionary<string, ConsoleKey>()
        {
            { "forward", ConsoleKey.W }, { "backward", ConsoleKey.S }, { "left", ConsoleKey.A }, { "right", ConsoleKey.D },
            { "swingUp", ConsoleKey.UpArrow }, { "swingDown", ConsoleKey.DownArrow }, { "swingLeft", ConsoleKey.LeftArrow },
            { "swingRight", ConsoleKey.RightArrow }, { "itemsLeft", ConsoleKey.Q}, { "itemsRight", ConsoleKey.E}
        };

        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            StartMenu();
            int s = MapSize();

            //TODO: better weapon adding to mobs
            mobDict["Player"].ItemsList.Add(itemDict["Stick"]);
            mobDict["Player"].ItemsList.Add(itemDict["Wall"]);
            mobDict["Player"].ItemsList.Add(itemDict["Bridge"]);
            mobDict["Player"].SelectedItem = 1;

            map = new string[s];
            objectMap = new StringBuilder[s];
            objectMapObjectsInfo = new List<Weapon>();
            GenerateMap(map);

            ulong gameTick = 0; //this is so that stuff like taking damage, drowning, ... is a bit slower than the almost instant game refresh
            while (mobDict["Player"].Alive == true)
            {
                if (Console.KeyAvailable)
                {
                    var keyPressed = Console.ReadKey().Key;
                    int playerYPos = mobDict["Player"].YPos, playerXPos = mobDict["Player"].XPos;

                    try
                    {
                        //we can't use switch, because it requires constant values, i.e. not variables, so with switch you can't customise button mappings
                        if (keyPressed == keyBindingsDict["forward"] && playerYPos > 0)
                        {
                            if (objectMap[playerYPos - 1][playerXPos] == '\0') mobDict["Player"].YPos--;
                            else if (objectMapObjectsInfo.Single(o => o.YPos == playerYPos - 1 && o.XPos == playerXPos).WalkOver) mobDict["Player"].YPos--;
                        }

                        else if (keyPressed == keyBindingsDict["backward"] && playerYPos < map.Length - 1)
                        {
                            if (objectMap[playerYPos + 1][playerXPos] == '\0') mobDict["Player"].YPos++;
                            else if (objectMapObjectsInfo.Single(o => o.YPos == playerYPos + 1 && o.XPos == playerXPos).WalkOver) mobDict["Player"].YPos++;
                        }

                        else if (keyPressed == keyBindingsDict["left"] && playerXPos > 0)
                        {
                            if (objectMap[playerYPos][playerXPos - 1] == '\0') mobDict["Player"].XPos--;
                            else if (objectMapObjectsInfo.Single(o => o.YPos == playerYPos && o.XPos == playerXPos - 1).WalkOver) mobDict["Player"].XPos--;
                        }

                        else if (keyPressed == keyBindingsDict["right"] && playerXPos < map.Length * 2 - 1)
                        {
                            if (objectMap[playerYPos][playerXPos + 1] == '\0') mobDict["Player"].XPos++;
                            else if (objectMapObjectsInfo.Single(o => o.YPos == playerYPos && o.XPos == playerXPos + 1).WalkOver) mobDict["Player"].XPos++;
                        }

                        else if (keyPressed == keyBindingsDict["itemsLeft"])
                        { if (mobDict["Player"].SelectedItem > 1) mobDict["Player"].SelectedItem--; }

                        else if (keyPressed == keyBindingsDict["itemsRight"])
                        { if (mobDict["Player"].SelectedItem < mobDict["Player"].Slots) mobDict["Player"].SelectedItem++; }

                        else if (keyPressed == keyBindingsDict["swingLeft"])
                        { mobDict["Player"].ItemsList[mobDict["Player"].SelectedItem - 1].Direction = "left"; } // 1

                        else if (keyPressed == keyBindingsDict["swingRight"])
                        { mobDict["Player"].ItemsList[mobDict["Player"].SelectedItem - 1].Direction = "right"; } // 2

                        else if (keyPressed == keyBindingsDict["swingUp"])
                        { mobDict["Player"].ItemsList[mobDict["Player"].SelectedItem - 1].Direction = "up"; } // 3

                        else if (keyPressed == keyBindingsDict["swingDown"])
                        { mobDict["Player"].ItemsList[mobDict["Player"].SelectedItem - 1].Direction = "down"; } // 4
                    }
                    catch (Exception) { }
                }

                if(gameTick % 2 == 0)
                {
                    MobMovement();
                }

                if (gameTick % 3 == 0)
                {
                    DoBiomeDamage(gameTick);
                }

                if (gameTick % 60 == 0)
                {
                    foreach (var mob in mobDict.Values.Where(m => m.FightingWithPlayer))
                    { mob.FightingWithPlayer = false; }

                    foreach (var breakable in itemDict.Values.Where(b => b.BrokenByPlayer))
                    { breakable.BrokenByPlayer = false; }
                }

                ReWriteScreen(map);
                gameTick++;

                foreach(var deadMob in mobDict.Values.Where(m => m.Health <= 0))
                { deadMob.Alive = false; }

                for(int i = 0; i < 34000000; i++)
                { } //replace of Thread.Sleep(80); because we haven't learned about thread stuff for now
            }

            Console.WriteLine("GAME OVER. YOU DIED.");
            Console.WriteLine($"\r\nPress any key to end the program.");
            ConsoleKey endProgram = Console.ReadKey().Key;
        }

        private static void MobMovement()
        {
            var rndDirection = new Random();

            foreach (var mob in mobDict.Values.Where(m => m.Name != "Player" && m.Alive == true))
            {
                int movementChooser = rndDirection.Next(20); //number is bigger than cases, so mob has a higher chance of staying at the same place rather than movning

                if (biomeDict.Values.Where(b => b.Floor == map[mob.YPos][mob.XPos]).Single().Damage > 0 && !objectMapObjectsInfo.Any(o => o.XPos == mob.XPos && o.YPos == mob.YPos && o.WalkOver))
                {
                    //this so if a mob is presented with more than one viable option, it doesn't always do the same thing
                    byte[] randomOrder = new byte[] { 1, 2, 3, 4}; 
                    randomOrder = randomOrder.OrderBy(x => rndDirection.Next()).ToArray();

                    foreach(var num in randomOrder)
                    {
                        try
                        {
                            switch (num)
                            {
                                case 1:
                                    if (biomeDict.Values.Where(b => b.Floor == map[mob.YPos][mob.XPos - 1]).Single().Damage == 0) movementChooser = 3;
                                    break;
                                case 2:
                                    if (biomeDict.Values.Where(b => b.Floor == map[mob.YPos][mob.XPos + 1]).Single().Damage == 0) movementChooser = 4;
                                    break;
                                case 3:
                                    if (biomeDict.Values.Where(b => b.Floor == map[mob.YPos - 1][mob.XPos]).Single().Damage == 0) movementChooser = 1;
                                    break;
                                case 4:
                                    if (biomeDict.Values.Where(b => b.Floor == map[mob.YPos + 1][mob.XPos]).Single().Damage == 0) movementChooser = 2;
                                    break;
                            }
                        }
                        catch (Exception)
                        { }
                    }
                }

                switch (movementChooser) 
                {
                    case 1: if (mob.YPos > 0 && (objectMap[mob.YPos - 1][mob.XPos] == '\0' || objectMapObjectsInfo.Single(o => o.XPos == mob.XPos && o.YPos == mob.YPos - 1).WalkOver)) mob.YPos--; break;
                    case 2: if (mob.YPos < map.Length - 1 && (objectMap[mob.YPos + 1][mob.XPos] == '\0' || objectMapObjectsInfo.Single(o => o.XPos == mob.XPos && o.YPos == mob.YPos + 1).WalkOver)) mob.YPos++; break;
                    case 3: if (mob.XPos > 0 && (objectMap[mob.YPos][mob.XPos - 1] == '\0' || objectMapObjectsInfo.Single(o => o.XPos == mob.XPos - 1 && o.YPos == mob.YPos).WalkOver)) mob.XPos--; break;
                    case 4: if (mob.XPos < map.Length * 2 - 1 && (objectMap[mob.YPos][mob.XPos + 1] == '\0' || objectMapObjectsInfo.Single(o => o.XPos == mob.XPos + 1 && o.YPos == mob.YPos).WalkOver)) mob.XPos++; break;
                }
            }
        }

        private static void DoBiomeDamage(ulong gameTick)
        {
            foreach (var mob in mobDict.Values.Where(m => m.Alive == true))
            {
                var biome = biomeDict.Values.Where(b => b.Floor == map[mob.YPos][mob.XPos]).Single();

                if (biome.Damage > 0 && objectMap[mob.YPos][mob.XPos] == '\0')
                {
                    if (biome.Suffocate && mob.LungCapacity > 0) mob.LungCapacity--;
                    else mob.Health -= biome.Damage;
                }
                else
                {
                    if (mob.LungCapacity < mob.MaxLungCapacity) mob.LungCapacity++;
                    else if (mob.Health < mob.MaxHealth && gameTick % 10 == 0) mob.Health++;
                }
            }
        }

        private static void ReWriteScreen(string[] map)
        {
            Console.Clear();

            for (int row = 0; row < map.Length; row++)
            {
                //because a map line can be max 2 biomes, the first half is determined by the first char, and the second half by the last char
                Console.ForegroundColor = biomeDict.Values.First(x => x.Floor == map[row].First()).Color; 
                Console.Write(map[row].Substring(0, map[row].LastIndexOf(map[row].First()) + 1));

                Console.ForegroundColor = biomeDict.Values.First(x => x.Floor == map[row].Last()).Color;
                Console.Write(map[row].TrimStart(map[row].First()));

                for (int col = 0; col < objectMap.Length * 2; col++)
                {
                    Console.SetCursorPosition(col, row);

                    if (objectMap[row][col] != '\0')
                    {
                        Weapon currItem = objectMapObjectsInfo.Single(w => w.XPos == col && w.YPos == row);

                        if (currItem.Health <= 0)
                        {
                            objectMap[row][col] = '\0';
                            objectMapObjectsInfo.Remove(currItem);
                        }
                        else
                        {
                            Console.ForegroundColor = itemDict.Values.First(x => x.SkinUp == objectMap[row][col]).Color;
                            Console.Write(objectMap[row][col]);
                        }
                    }
                }
                Console.WriteLine();

                foreach (var mob in mobDict.Values.Where(m => m.Alive == true))
                {
                    //this prints all mobs that are on the line that was just drawn, so that there is less cursor "jumping"
                    if (mob.YPos == row) mob.Print();

                    //prints weapon when the line under the mob is drawn (so that the weapon is not overwritten by the biome) or is on the last line, also for less cursor "jumping"
                    if (mob.ItemsList.Count > 0 && (row == mob.YPos + 1 || row == map.Length - 1))
                    {
                        try
                        {
                            var selectedItem = mob.ItemsList[mob.SelectedItem - 1];

                            if (!selectedItem.Placable) selectedItem.PrintAndDoDamage(mob, mobDict, objectMapObjectsInfo, map.Length * 2, map.Length);

                            else if (selectedItem.Direction != null)
                            {
                                selectedItem.WeaponPosition(mob, map.Length * 2, map.Length);

                                if (objectMap[selectedItem.YPos][selectedItem.XPos] == '\0')
                                {
                                    objectMapObjectsInfo.Add(new Weapon(selectedItem.Name, selectedItem.Health, selectedItem.HealthDamage, selectedItem.BreakDamage, selectedItem.SkinLeft, selectedItem.SkinRight, selectedItem.SkinUp, selectedItem.SkinDown, selectedItem.Color, true, selectedItem.WalkOver, 1, selectedItem.XPos, selectedItem.YPos));
                                    objectMap[selectedItem.YPos][selectedItem.XPos] = selectedItem.SkinUp;
                                    selectedItem.Amount--;

                                    if (selectedItem.Amount == 0) mob.ItemsList.Remove(selectedItem);
                                }
                            }

                            selectedItem.Direction = null;
                        } catch (Exception) { }
                    }
                }
                Console.SetCursorPosition(0, row + 1);
            }

            Console.SetCursorPosition(0, map.Length + 1); //sets cursor at the very end
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(mobDict["Player"].Stats());
            mobDict["Player"].PrintInventory();

            foreach(var mob in mobDict.Values.Where(m => m.FightingWithPlayer && m.Alive))
            {
                Console.WriteLine(mob.Stats());
            }

            foreach(var breakable in objectMapObjectsInfo.Where(b => b.BrokenByPlayer))
            {
                Console.WriteLine(breakable.Stats());
            }

            Console.CursorVisible = false;
        }

        private static void StartMenu()
        {
            Console.CursorVisible = false;
            int cursorYPos = 0;
            bool stop = false;

            while (!stop)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(" New game");
                Console.WriteLine(" Settings");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.SetCursorPosition(0, cursorYPos);
                Console.Write('>');
                Console.CursorVisible = false;

                var keyPressed = Console.ReadKey().Key;

                switch (keyPressed)
                {
                    case ConsoleKey.UpArrow: if (cursorYPos == 1) cursorYPos--; break;
                    case ConsoleKey.DownArrow: if (cursorYPos == 0) cursorYPos++; break;
                    case ConsoleKey.Enter: stop = true; break;
                }
            }

            if (cursorYPos == 1) Settings();
        }

        private static void Settings()
        {
            Mob player = mobDict["Player"];
            bool stop = false;
            int cursorYPos = 1; // tracks cursor position

            while (!stop)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Settings:");
                Console.WriteLine($" Player char: {player.Body}");
                Console.WriteLine($" Player color: {player.Color}");
                Console.WriteLine(" Key Bindings Settings");
                Console.WriteLine(" Back");

                Console.SetCursorPosition(0, cursorYPos);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write('>');
                Console.CursorVisible = false;

                var keyPressed = Console.ReadKey().Key;

                switch (keyPressed)
                {
                    case ConsoleKey.UpArrow: if (cursorYPos > 1) cursorYPos--; break;
                    case ConsoleKey.DownArrow: if (cursorYPos < 4) cursorYPos++; break;
                    case ConsoleKey.Enter:
                        {
                            switch (cursorYPos)
                            {
                                case 1:
                                    {
                                        Console.SetCursorPosition(14, 1);
                                        Console.CursorVisible = true;

                                        player.Body = Console.ReadKey().KeyChar;

                                        Console.CursorVisible = false;
                                        Console.SetCursorPosition(0, 1);

                                    } break;
                                case 2:
                                    {
                                        player.Color = ColorPicker(player.Color);
                                    } break;
                                case 3:  
                                case 4: stop = true; break;
                            }
                        }
                        break;
                }
            }

            //this is not in the switch, so when you exit key bindings and then exit settings, you go into the start menu, you don't continue in the settings loop
            if (cursorYPos == 3) KeyBindings(); 
            else StartMenu();
        }

        private static void KeyBindings()
        {
            bool stop = false;
            int cursorYPos = 1;
            int cursosXPos = 0; //used to change cursor position when changing a button
            string keyToChange = null;
            
            while (!stop)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.WriteLine("Key Bindings");
                Console.WriteLine($" Move forward: {keyBindingsDict["forward"]}");
                Console.WriteLine($" Move backward: {keyBindingsDict["backward"]}");
                Console.WriteLine($" Move left: {keyBindingsDict["left"]}");
                Console.WriteLine($" Move right: {keyBindingsDict["right"]}");
                Console.WriteLine($" Swing weapon up: {keyBindingsDict["swingUp"]}");
                Console.WriteLine($" Swing weapon down: {keyBindingsDict["swingDown"]}");
                Console.WriteLine($" Swing weapon left: {keyBindingsDict["swingLeft"]}");
                Console.WriteLine($" Swing weapon right: {keyBindingsDict["swingRight"]}");
                Console.WriteLine($" Scroll items to the left: {keyBindingsDict["itemsLeft"]}");
                Console.WriteLine($" Scroll items to the right: {keyBindingsDict["itemsRight"]}");
                Console.WriteLine(" Back");

                Console.SetCursorPosition(0, cursorYPos);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write('>');
                Console.CursorVisible = false;

                var keyPressed = Console.ReadKey().Key;

                switch (keyPressed)
                {
                    case ConsoleKey.UpArrow: if (cursorYPos > 1) cursorYPos--; break;
                    case ConsoleKey.DownArrow: if (cursorYPos < 11) cursorYPos++; break;
                    case ConsoleKey.Enter:
                        {
                            switch (cursorYPos)
                            {
                                case 1: cursosXPos = 15; keyToChange = "forward"; break;
                                case 2: cursosXPos = 16; keyToChange = "backward"; break;
                                case 3: cursosXPos = 12; keyToChange = "left"; break;
                                case 4: cursosXPos = 13; keyToChange = "right"; break;
                                case 5: cursosXPos = 18; keyToChange = "swingUp"; break;
                                case 6: cursosXPos = 20; keyToChange = "swingDown"; break;
                                case 7: cursosXPos = 20; keyToChange = "swingLeft"; break;
                                case 8: cursosXPos = 21; keyToChange = "swingRight"; break;
                                case 9: cursosXPos = 27; keyToChange = "itemsLeft"; break;
                                case 10: cursosXPos = 28; keyToChange = "itemsRight"; break;
                                default: stop = true; break;
                            }

                            if (cursorYPos != 11)
                            {
                                Console.SetCursorPosition(cursosXPos, cursorYPos);
                                Console.CursorVisible = true;

                                keyBindingsDict[keyToChange] = Console.ReadKey().Key;

                                Console.CursorVisible = false;
                                Console.SetCursorPosition(0, cursorYPos);
                            }
                        }
                        break;
                }
            }

            Settings();
        }

        private static int MapSize()
        {
            int size = 0;
            int cursorYPos = 1;

            while (size == 0)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Choose map size:");
                Console.WriteLine("-Small  (5 x 10)");
                Console.WriteLine("-Medium (10 x 20)");
                Console.WriteLine("-Large  (20 x 40) !FLICKER!");

                Console.SetCursorPosition(0, cursorYPos);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write('>');
                Console.CursorVisible = false;

                var keyPressed = Console.ReadKey().Key;

                switch (keyPressed)
                {
                    case ConsoleKey.UpArrow: if (cursorYPos > 1) cursorYPos--; break;
                    case ConsoleKey.DownArrow: if (cursorYPos < 3) cursorYPos++; break;
                    case ConsoleKey.Enter:
                        {
                            switch (cursorYPos)
                            {
                                case 1: size = 5; break;
                                case 2: size = 10; break;
                                case 3: size = 20; break;
                            }
                        }
                        break;
                }
            }
            return size;
        }

        private static void GenerateMap(string[] map)
        {
            Random randomNumber = new Random();

            //map is composed of rows and every row has 1 or 2 biomes in it
            for (int i = 0; i < map.Length; i++)
            {
                Biome currBiome = RandomBiome(randomNumber);
                map[i] = new string(currBiome.Floor, map.Length * 2 - randomNumber.Next(map.Length + 1));

                currBiome = RandomBiome(randomNumber);
                map[i] = map[i] + new string(currBiome.Floor, map.Length * 2 - map[i].Length);

                objectMap[i] = new StringBuilder(new string('\0', objectMap.Length * 2));
            }
        }

        private static Biome RandomBiome(Random randomNumber)
        {
            Biome currBiome = new Biome();

            if (biomeDict.Count() == 0)
            { throw new ArgumentException("There must be atleast one biome in the biome dictionary"); }

            while (!biomeDict.Values.Contains(currBiome)) //mainly for tests, so you can remove biomes and it will still generate a map
            {
                try
                {
                    if (randomNumber.Next(4) > 0) // 3/4 chance that the biome will not hurt the mob
                    {
                        currBiome = biomeDict.Where(x => x.Value.Damage == 0).ElementAt(randomNumber.Next(biomeDict.Count(x => x.Value.Damage == 0))).Value;
                    }
                    else
                    {
                        if (randomNumber.Next(5) > 0) // 4/5 chance that the biome will suffocate and hurt
                        {
                            currBiome = biomeDict.Where(x => x.Value.Damage > 0 && x.Value.Suffocate)
                                        .ElementAt(randomNumber.Next(biomeDict.Count(x => x.Value.Damage > 0 && x.Value.Suffocate))).Value;
                        }
                        else
                        {
                            currBiome = biomeDict.Where(x => x.Value.Damage > 0 && !x.Value.Suffocate)
                                       .ElementAt(randomNumber.Next(biomeDict.Count(x => x.Value.Damage > 0 && !x.Value.Suffocate))).Value;
                        }
                    }
                } catch { }
            }

            return currBiome;
        }

        private static ConsoleColor ColorPicker(ConsoleColor defaultColor)
        {
            int y = 1; //tracks cursor position

            while (true)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Settings:");

                Console.ForegroundColor = defaultColor;
                Console.WriteLine($" Default");

                //not using a cycle, because where the console colors are situated, is a bit too advanced (this game's idea is to mostly train my knowledge)
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine($" Black");

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($" Blue");

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($" Cyan");

                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine($" Dark Blue");

                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine($" Dark Cyan");

                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine($" Dark Gray");

                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine(" Dark Green");

                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine(" Dark Magenta");

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(" Dark Red");

                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(" Dark Yellow");

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(" Gray");

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(" Green");

                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(" Magenta");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" Red");

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" White");

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(" Yellow");

                Console.SetCursorPosition(0, y);
                Console.Write('>');
                Console.CursorVisible = false;

                var keyPressed = Console.ReadKey().Key;

                switch (keyPressed)
                {
                    case ConsoleKey.UpArrow: if (y > 1) y--; break;
                    case ConsoleKey.DownArrow: if (y < 17) y++; break;
                    case ConsoleKey.Enter:
                        {
                            switch (y)
                            {
                                case 1: return default;
                                case 2: return ConsoleColor.Black;
                                case 3: return ConsoleColor.Blue;
                                case 4: return ConsoleColor.Cyan;
                                case 5: return ConsoleColor.DarkBlue;
                                case 6: return ConsoleColor.DarkCyan;
                                case 7: return ConsoleColor.DarkGray;
                                case 8: return ConsoleColor.DarkGreen;
                                case 9: return ConsoleColor.DarkMagenta;
                                case 10: return ConsoleColor.DarkRed;
                                case 11: return ConsoleColor.DarkYellow;
                                case 12: return ConsoleColor.Gray;
                                case 13: return ConsoleColor.Green;
                                case 14: return ConsoleColor.Magenta;
                                case 15: return ConsoleColor.Red;
                                case 16: return ConsoleColor.White;
                                case 17: return ConsoleColor.Yellow;
                            }
                        }
                        break;
                }
            }
        }
    }
}
