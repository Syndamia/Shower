﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Biome
    {
        private string name;
        private char floor;
        private ConsoleColor color;
        private ushort damage; 
        private bool suffocate; //if true it will firstly drain the air (lungCapacity) variable and then will drain health

        public string Name
        {
            set { name = value; }
            get { return name; }
        }

        public char Floor
        {
            set { floor = value; }
            get { return floor; }
        }

        public ConsoleColor Color
        {
            set { color = value; }
            get { return color; }
        }

        public ushort Damage
        {
            set { damage = value; }
            get { return damage; }
        }

        public bool Suffocate
        {
            set { suffocate = value; }
            get { return suffocate; }
        }

        public Biome() : this(null, '\0', ConsoleColor.Black, 0, false)
        { }

        public Biome(string name, char floor, ConsoleColor color) : this(name, floor, color, 0, false)
        { }

        public Biome(string name, char floor, ConsoleColor color, ushort damage) : this(name, floor, color, damage, false)
        { }

        public Biome(string name, char floor, ConsoleColor color, ushort damage, bool suffocate)
        {
            Name = name;
            Floor = floor;
            Color = color;
            Damage = damage;
            Suffocate = suffocate;
        }
    }
}
