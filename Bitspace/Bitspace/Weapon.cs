﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Weapon
    {
        private int xPos;
        private int yPos;

        private string name;
        private int maxHealth;
        private int health;

        private ushort healthDamage;
        private ushort breakDamage;
        public bool BrokenByPlayer { get; set; }

        private ConsoleColor color;
        private string direction;

        private char skinLeft;
        private char skinRight;
        private char skinUp;
        private char skinDown;

        private bool placable;
        private bool walkOver;
        private byte amount;

        public void PrintAndDoDamage(Mob fightingMob, Dictionary<string, Mob> mobDict, List<Weapon> objectMapObjectsInfo, int maxX, int maxY)
        {
            Print(fightingMob, maxX, maxY);
            DoHealthDamage(mobDict, fightingMob);
            DoBreakDamage(objectMapObjectsInfo, fightingMob);
        }

        private void Print(Mob fightingMob, int maxX, int maxY)
        {
            WeaponPosition(fightingMob, maxX, maxY);

            if (WeaponIsSwung())
            {
                Console.ForegroundColor = Color;
                Console.SetCursorPosition(XPos, YPos);
                Console.Write(SwordChar());
            }
        }

        public string Stats()
        {
            return $"{Name} | Health: {Health} | X: {XPos} | Y: {YPos}";
        }

        private void DoHealthDamage(Dictionary<string, Mob> mobDict, Mob fightingMob)
        {
            if (WeaponIsSwung())
            {
                foreach (var mob in mobDict.Values.Where(m => m.XPos == XPos && m.YPos == YPos))
                {
                    if (fightingMob.Name == "Player") mob.FightingWithPlayer = true;

                    for (ushort d = HealthDamage; d > 0 && mob.Health > 0; d--)
                    {
                        if (HealthDamage <= mob.Armour / 4) { } //does nothing, all damage that is below a fourth of the armour is nullified
                        else if (mob.Armour > 0 && d >= HealthDamage * 2 / 3) mob.Armour--;
                        else mob.Health--;
                    }
                }
            }
        }

        private void DoBreakDamage(List<Weapon> objectMapObjectsInfo, Mob fightingMob)
        {
            if (WeaponIsSwung())
            {
                foreach (var obj in objectMapObjectsInfo.Where(o => (o.XPos == XPos && o.YPos == YPos) && o.Health > 0 && o != fightingMob.ItemsList[fightingMob.SelectedItem - 1]))
                {
                    if (fightingMob.Name == "Player") obj.BrokenByPlayer = true;

                    obj.Health -= breakDamage;

                    if (obj.Health <= 0) fightingMob.AddItemToInventory(obj);
                }
            }
        }

        private bool WeaponIsSwung()
        {
            return (SwordChar() != '\0') && (YPos != -1) && (XPos != -1);
        }

        public void WeaponPosition(Mob mobWithWeapon, int maxX, int maxY)
        {
            XPos = -1; YPos = -1;

            switch (Direction)
            {
                case "left": if (mobWithWeapon.XPos > 0) XPos = mobWithWeapon.XPos - 1; YPos = mobWithWeapon.YPos; break;
                case "right": if (mobWithWeapon.XPos < maxX - 1) XPos = mobWithWeapon.XPos + 1; YPos = mobWithWeapon.YPos; break;
                case "up": if (mobWithWeapon.YPos > 0) YPos = mobWithWeapon.YPos - 1; XPos = mobWithWeapon.XPos; break;
                case "down": if (mobWithWeapon.YPos < maxY - 1) YPos = mobWithWeapon.YPos + 1; XPos = mobWithWeapon.XPos; break;
            }
        }

        private char SwordChar()
        {
            switch (Direction)
            {
                case "left": return SkinLeft;
                case "right": return SkinRight;
                case "up": return SkinUp;
                case "down": return SkinDown;
                default: return '\0'; // \0 is the char equivalent of null
            }
        }

        public Weapon(string name, int health, ushort healthDamage, ushort breakDamage, char tetraChar, bool placeable, bool walkOver, byte amount) : this(name, health, healthDamage, breakDamage, tetraChar, tetraChar, tetraChar, tetraChar, ConsoleColor.Gray, placeable, walkOver, amount, 0, 0)
        { }

        public Weapon(string name, int health, ushort healthDamage, ushort breakDamage, char tetraChar, ConsoleColor color, bool placeable, bool walkOver, byte amount) : this(name, health, healthDamage, breakDamage, tetraChar, tetraChar, tetraChar, tetraChar, color, placeable, walkOver, amount, 0, 0)
        { }

        public Weapon(string name, int health, ushort healthDamage, ushort breakDamage, char leftRight, char upDown, bool placeable, bool walkOver, byte amount) : this(name, health, healthDamage, breakDamage, leftRight, leftRight, upDown, upDown, ConsoleColor.Gray, placeable, walkOver, amount, 0, 0)
        { }

        public Weapon(string name, int health, ushort healthDamage, ushort breakDamage, char leftRight, char upDown, ConsoleColor color, bool placeable, bool walkOver, byte amount) : this(name, health, healthDamage, breakDamage, leftRight, leftRight, upDown, upDown, color, placeable, walkOver, amount, 0, 0)
        { }

        public Weapon(string name, int health, ushort healthDamage, ushort breakDamage, char skinLeft, char skinRight, char skinUp, char skinDown, ConsoleColor color, bool placable, bool walkOver, byte amount) : this(name, health, healthDamage, breakDamage, skinLeft, skinRight, skinUp, skinDown, color, placable, walkOver, amount, 0, 0)
        { }

        public Weapon(string name, int health, ushort healthDamage, ushort breakDamage, char skinLeft, char skinRight, char skinUp, char skinDown, ConsoleColor color, bool placable, bool walkOver, byte amount, int xPos, int yPos)
        {
            Health = health;
            MaxHealth = Health;
            HealthDamage = healthDamage;
            BreakDamage = breakDamage;
            SkinLeft = skinLeft;
            SkinRight = skinRight;
            SkinUp = skinUp;
            SkinDown = skinDown;
            Name = name;
            Color = color;
            Amount = amount;
            Placable = placable;
            WalkOver = walkOver;
            XPos = xPos;
            YPos = yPos;
        }

        public ushort HealthDamage { get => healthDamage; set => healthDamage = value; }
        public char SkinLeft { get => skinLeft; set => skinLeft = value; }
        public char SkinRight { get => skinRight; set => skinRight = value; }
        public char SkinUp { get => skinUp; set => skinUp = value; }
        public char SkinDown { get => skinDown; set => skinDown = value; }
        public string Direction { get => direction; set => direction = value; }
        public string Name { get => name; set => name = value; }
        public ConsoleColor Color { get => color; set => color = value; }
        public byte Amount { get => amount; set => amount = value; }
        public bool Placable { get => placable; set => placable = value; }
        public int XPos { get => xPos; private set => xPos = value; }
        public int YPos { get => yPos; private set => yPos = value; }
        public int Health { get => health; set => health = value; }
        public ushort BreakDamage { get => breakDamage; set => breakDamage = value; }
        public bool WalkOver { get => walkOver; set => walkOver = value; }
        public int MaxHealth { get => maxHealth; set => maxHealth = value; }
    }
}
