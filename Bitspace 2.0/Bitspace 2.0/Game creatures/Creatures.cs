﻿using Bitspace_2._0.Game_creatures;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Bitspace_2._0.Game_creatures {
    public static class Creatures { //when adding new collections, update properties at the bottom
        public static Dictionary<string, Player> Players = new Dictionary<string, Player>() {
            { "Player1", new Player("The player", '@', ConsoleColor.White, 10) }
        };

        public static Dictionary<string, Animal> Animals = new Dictionary<string, Animal>() {
            { "Smiley", new Animal("Smiley", 'U', ConsoleColor.Magenta, 3) }
        };

        public static List<Mob> AllAliveCreatures {
            get {
                var toReturn = AllAliveNPCCreatures;

                toReturn.AddRange(Players.Values.Where(x => x.IsAlive()));

                return toReturn;
            }
        }

        public static List<Mob> AllAliveNPCCreatures {
            get {
                return new List<Mob>(Animals.Values.Where(x => x.IsAlive()));
            }
        }
    }
}
