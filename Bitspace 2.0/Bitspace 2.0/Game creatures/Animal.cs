﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitspace_2._0.Game_creatures {
    public class Animal : Mob {
        public Animal(string name, char body, ConsoleColor color, int maxHealth)
        : base(name, body, color, 20, maxHealth, 10, 0, 1) { }

        public Animal(string name, char body, ConsoleColor color, int maxHealth, int maxLungCapacity)
        : base(name, body, color, 20, maxHealth, maxLungCapacity, 0, 1) { }

        public Animal(string name, char body, ConsoleColor color, int defaultMovementFactor, int maxHealth, int maxLungCapacity)
        : base(name, body, color, defaultMovementFactor, maxHealth, maxLungCapacity, 0, 1) { }

        public Animal(string name, char body, ConsoleColor color, int defaultMovementFactor, int maxHealth, int maxLungCapacity, int maxArmour, int inventorySize)
        : base(name, body, color, defaultMovementFactor, maxHealth, maxLungCapacity, maxArmour, inventorySize) { }
    }
}
