﻿using System;
using System.Linq;

namespace Bitspace_2._0.Game_creatures {
    public class Player : Mob {
        public void InputAction() {
            var pressedKey = Console.ReadKey().Key;
            
            if (KeyBindings.Movement.ContainsValue(pressedKey)) {
                switch (KeyBindings.Movement.First(kp => kp.Value == pressedKey).Key) {
                    case "Forwards": Move(1); break;
                    case "Backwards": Move(2); break;
                    case "Left": Move(3); break;
                    case "Right": Move(4); break;
                }
            } 
            else if (KeyBindings.Inventory.ContainsValue(pressedKey)) {
                switch (KeyBindings.Inventory.First(kp => kp.Value == pressedKey).Key) {
                    case "Next": if (this.selectedItem < this.inventorySize - 1) this.selectedItem++; break;
                    case "Previous": if (this.selectedItem > 0) this.selectedItem--; break;
                }
            }
        }

        public Player(string name, char body, ConsoleColor color, int maxHealth) 
        : base(name, body, color, 20, maxHealth, 10, 0, 10) { }

        public Player(string name, char body, ConsoleColor color, int maxHealth, int maxLungCapacity) 
        : base(name, body, color, 20, maxHealth, maxLungCapacity, 0, 10) { }

        public Player(string name, char body, ConsoleColor color, int defaultMovementFactor, int maxHealth, int maxLungCapacity) 
        : base(name, body, color, defaultMovementFactor, maxHealth, maxLungCapacity, 0, 10) { }

        public Player(string name, char body, ConsoleColor color, int defaultMovementFactor, int maxHealth, int maxLungCapacity, int maxArmour) 
        : base(name, body, color, defaultMovementFactor, maxHealth, maxLungCapacity, maxArmour, 10) { }

        public Player(string name, char body, ConsoleColor color, int defaultMovementFactor, int maxHealth, int maxLungCapacity, int maxArmour, int inventorySize)
        : base(name, body, color, defaultMovementFactor, maxHealth, maxLungCapacity, maxArmour, inventorySize) { }
    }
}
