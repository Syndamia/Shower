﻿using Bitspace_2._0.Game_map;
using Bitspace_2._0.Game_items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bitspace_2._0.Game_creatures {
    abstract public class Mob { //short for mobile object
        protected static Random rndNum = new Random(); //the random class uses certain patters to it's randomness, this way it adds "another layer to the randomness"

        protected string name;
        protected char body;
        public ConsoleColor Color { get; set; }

        public int XPos { get; set; }
        public int YPos { get; set; }
        public int MovementFactor { get; set; }
        public int DefaultMovementFactor { get; protected set; }

        public int Health { get; set; }
        public int MaxHealth { get; protected set; }
        public int LungCapacity { get; set; }
        public int MaxLungCapacity { get; protected set; }
        public int Armour { get; protected set; }
        public int MaxArmour { get; protected set; }

        public Player fightingWithPlayer { get; set; }

        public Dictionary<string, int> ItemInventory { get; protected set; }
        protected int inventorySize;
        protected int selectedItem;

        public string PrintInventory() {
            var temp = new Item[inventorySize];
            foreach(Item toAdd in Items.AllItems.Where(i => this.ItemInventory.Keys.Any(ii => ii == i.Name))) {
                temp.Last(x => x == null) = toAdd;
            }
            return Inventory.PrintInventory(temp, selectedItem);
        }

        public void PickUpItem(string name) {
            ItemInventory.Add(name, Items.AllItems.First(i => i.Name == name).Quantity);
        }

        public void PickUpItem(string name, int quantity) {
            ItemInventory.Add(name, quantity);
        }

        public void ChooseRndMovement() {
            var randomDirection = rndNum.Next(MovementFactor); //the bigger it is, the less chance for a mob to move from it's current position

            Move(randomDirection);
        }

        public void UseItem(string pressedKeyBinding) {
            Item itemToUse = Items.AllItems.First(w => w.Name == this.ItemInventory.ElementAt(selectedItem).Key);

            if (Items.AllWeapons.Contains(itemToUse)) UseWeapon((Weapon)itemToUse, pressedKeyBinding); 
        }
        
        private void UseWeapon(Weapon weaponToUse, string pressedKeyBinding) {
            int wepXPos = this.XPos, wepYPos = this.YPos;

            switch (pressedKeyBinding) {
                case "UseForward": if (wepYPos > Map.MinY) wepYPos--; break;
                case "UseBackward": if (wepYPos < Map.MaxY) wepYPos++; break;
                case "UseLeft": if (wepXPos > Map.MinX) wepXPos--; break;
                case "UseRight": if (wepXPos < Map.MaxX) wepXPos++; break;
            }

            if (wepXPos != this.XPos || wepYPos != this.YPos) {
                Map.ItemLayer[wepYPos][wepXPos] = weaponToUse.Body;

                foreach(var cr in Creatures.AllAliveCreatures.Where(c => c.XPos == wepXPos && c.YPos == wepYPos)) {
                    weaponToUse.DoDirectDamage(cr);
                }
            }
        }

        protected void Move(int direction) {
            switch (direction) {
                case 1: //North
                    if (this.YPos > Map.MinY) YPos--;
                    break;
                case 2: //South
                    if (this.YPos < Map.MaxY) YPos++;
                    break;
                case 3: //West
                    if (this.XPos > Map.MinX) XPos--;
                    break;
                case 4: //East
                    if (this.XPos < Map.MaxX) XPos++;
                    break;
            }
        }

        public void GetAffectedByBiome(Biome currBiome) {
            if (HasFullLungs()) this.Heal(currBiome.HealPoints);

            if (currBiome.Suffocate) this.Suffocate();
            else this.TakeBreather();

            RecieveDamage(currBiome.DamagePoints);
        }

        public void Heal(int points) {
            if (this.Health < this.MaxHealth && IsAlive()) {
                Health += points;
                if (this.Health > this.MaxHealth) Health = MaxHealth;
            }
        }

        public void TakeBreather() {
            if (IsAlive() && !HasFullLungs()) {
                LungCapacity++;
            }
        }

        private bool HasFullLungs() {
            return LungCapacity == MaxLungCapacity;
        }

        public void Suffocate() {
            if (this.LungCapacity > 0) {
                this.LungCapacity--;
            }
            else if (IsAlive()) {
                LooseHealth(1);
            }
        }

        public void RecieveDamage(int points) {
            if (IsAlive()) {
                if (this.Armour > 0 && points > this.Armour / 4) { //all damage under a fourth of the armor doesn't affect mob
                    this.Armour -= 2 / 3 * points;
                    this.Health -= (int)Math.Ceiling(1M / 3 * points);

                    if (this.Armour < 0) {
                        this.Health += this.Armour; //health will go down because a + -b = a - b
                        this.Armour = 0;
                    }
                }
                else LooseHealth(points);
            }
        }

        public void LooseHealth(int points) {
            if (IsAlive()) {
                this.Health -= points;
            }
        }

        public bool IsAlive() {
            return this.Health > 0;
        }

        public Mob (string name, char body, ConsoleColor color, int maxHealth) 
        : this(name, body, color, 20, maxHealth, 10, 0, 3) { }

        public Mob (string name, char body, ConsoleColor color, int maxHealth, int maxLungCapacity)
        : this(name, body, color, 20, maxHealth, maxLungCapacity, 0, 3) { }

        public Mob(string name, char body, ConsoleColor color, int defaultMovementFactor, int maxHealth, int maxLungCapacity) 
        : this(name, body, color, defaultMovementFactor, maxHealth, maxLungCapacity, 0, 3) { }

        public Mob (string name, char body, ConsoleColor color, int defaultMovementFactor, int maxHealth, int maxLungCapacity, int maxArmour, int inventorySize) {
            this.Name = name;
            this.Body = body;
            this.Color = color;

            this.XPos = 0; this.YPos = 0; //temporary
            this.MovementFactor = this.DefaultMovementFactor = defaultMovementFactor;

            this.Health = this.MaxHealth = maxHealth;
            this.LungCapacity = this.MaxLungCapacity = maxLungCapacity;
            this.Armour = this.MaxArmour = maxArmour;

            this.ItemInventory = new Dictionary<string, int>(inventorySize);
            this.inventorySize = inventorySize;
            this.selectedItem = 0;
        }

        public override string ToString() {
            return $"{this.Name} | Health: {this.Health} | Armour: {this.Armour} | Air: {GenerateBreatheBar()} || X: {this.XPos} | Y: {this.YPos}";
        }

        private string GenerateBreatheBar() {
            var toReturn = new StringBuilder(new string('O', this.LungCapacity));

            for (int i = this.LungCapacity; i < this.MaxLungCapacity; i++) {
                toReturn.Append('*');
            }
            return toReturn.ToString();
        }

        public string Name {
            get { return this.name; }
            protected set {
                if (!value.Any(c => (c > 47 && c < 58) || (c > 64 && c < 91) || (c > 96 && c < 123) || c == '_' || c == ' ')) {
                    throw new ArgumentException("Player name can be composed only of small letters, big letters, numbers, underscores and spaces.");
                }
                this.name = value;
            } 
        }

        public char Body {
            get { return this.body; }
            protected set {
                if (value < 33 || value > 126) { //TODO: limit it even more (in the end)
                    throw new ArgumentException("Body character can only be from the ASCII printable characters (from 32 to 126), without the space character (32)");
                }
                this.body = value;
            }
        }
    }
}
