﻿using System;
using System.Collections.Generic;

namespace Bitspace_2._0 {
    static class KeyBindings {
        public static Dictionary<string, ConsoleKey> Movement = new Dictionary<string, ConsoleKey>() {
            { "Forwards", ConsoleKey.W },
            { "Backwards", ConsoleKey.S },
            { "Left", ConsoleKey.A },
            { "Right", ConsoleKey.D }
        };

        public static Dictionary<string, ConsoleKey> Inventory = new Dictionary<string, ConsoleKey>() {
            { "Next", ConsoleKey.E },
            { "Previous", ConsoleKey.Q }
        };

        public static Dictionary<string, ConsoleKey> Item = new Dictionary<string, ConsoleKey>() {
            { "UseForward", ConsoleKey.UpArrow },
            { "UseBackward", ConsoleKey.DownArrow },
            { "UseLeft", ConsoleKey.LeftArrow },
            { "UseRight", ConsoleKey.RightArrow }
        };
    }
}
