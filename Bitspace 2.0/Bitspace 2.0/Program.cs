﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bitspace_2._0.Game_map;
using Bitspace_2._0.Game_creatures;

namespace Bitspace_2._0 {
    class Program {
        static void Main(string[] args) {
            Console.CursorVisible = false;
            Menu.StartMenu();
            Console.Clear();

            Creatures.Players["Player1"].ItemInventory.Add("Stick", 1);
            for (ulong gameTick = 0; ; gameTick++, Thread.Sleep(80)) {
                if (Console.KeyAvailable) {
                    if (Creatures.Players.Count(p => p.Value.IsAlive()) > 0) {
                        Creatures.Players["Player1"].InputAction(); //temporary
                    } else {
                        break;
                    }
                }

                if (gameTick % 2 == 0) {
                    if (Creatures.AllAliveNPCCreatures.Count() > 0) {
                        foreach (var currNPC in Creatures.AllAliveNPCCreatures) {
                            currNPC.ChooseRndMovement();
                        }
                    }
                }

                if (gameTick % 4 == 0) {
                    if (Creatures.AllAliveCreatures.Count() > 0) {
                        foreach (var currCreature in Creatures.AllAliveCreatures) {
                            currCreature.GetAffectedByBiome(Map.Biomes.Values.First(b => b.Ground == Map.MapLayer[currCreature.YPos][currCreature.XPos]));
                        }
                    }
                }

                Graphics.ReWriteScreen();
            }
            Console.WriteLine("GAME OVER");
        }
    }
}
