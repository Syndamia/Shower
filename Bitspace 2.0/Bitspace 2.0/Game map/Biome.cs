﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitspace_2._0.Game_map {
    public class Biome {
        private string name;

        private char ground;
        public ConsoleColor Color { get; private set; }

        public bool Suffocate { get; private set; }
        //these values are per game tick / every time they are called
        public int DamagePoints { get; private set; }
        public int HealPoints { get; private set; }

        public Biome(string name, char ground, ConsoleColor color) : this(name, ground, color, false, 0, 0) { }

        public Biome(string name, char ground, ConsoleColor color, bool suffocate) : this(name, ground, color, suffocate, 0, 0) { }

        public Biome(string name, char ground, ConsoleColor color, int damage, int heal) : this(name, ground, color, false, damage, heal) { }

        public Biome(string name, char ground, ConsoleColor color, bool suffocate, int damage, int heal) {
            this.Name = name;
            this.Ground = ground;
            this.Color = color;
            this.Suffocate = suffocate;
            this.DamagePoints = damage;
            this.HealPoints = heal;
        }

        public string Name {
            get { return name; }
            private set {
                if (!value.Any(c => (c > 47 && c < 58) || (c > 64 && c < 91) || (c > 96 && c < 123) || c == '_')) {
                    throw new ArgumentException("Biome name can be composed only of small letters, big letters, numbers and the underscore character");
                }
                this.name = value;
            }
        }

        public char Ground {
            get { return ground; }
            private set {
                if (value < 33 || value > 126) {
                    throw new ArgumentException("Biome ground character can only be from the ASCII printable characters (from 32 to 126), without the space character (32)");
                }
                this.ground = value;
            }
        }
    }
}
