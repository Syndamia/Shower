﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bitspace_2._0.Game_map {
    static class Map {
        public static StringBuilder[] MobLayer { get; private set; }
        public static StringBuilder[] ItemLayer { get; private set; }
        public static StringBuilder[] MapLayer { get; private set; }

        //hardcoded temporarely
        public static int MinX = 0;
        public static int MaxX;
        public static int MinY = 0;
        public static int MaxY;

        public static Dictionary<string, Biome> Biomes = new Dictionary<string, Biome>() {
            { "Grass", new Biome("Grass", ',', ConsoleColor.Green, 0, 1) },
            { "Water", new Biome("Water", '-', ConsoleColor.Blue, true, 0, 0) },
            { "Lava", new Biome("Lava", '#', ConsoleColor.Red, true, 5, 0) },
            { "Rock_spikes", new Biome("Rock_spikes", ';', ConsoleColor.Gray, 2, 1) }
        };

        public static void Generate(int width, int height) {
            MaxX = width - 1;//temporary
            MaxY = height - 1;//temporary

            MobLayer = NewBlanckSBArray(width, height);
            ItemLayer = NewBlanckSBArray(width, height);
            MapLayer = GenerateMapLayer(width, height);
        }

        private static StringBuilder[] NewBlanckSBArray(int width, int height) {
            StringBuilder[] sBArray = new StringBuilder[height];

            for (int row = 0; row < height; row++) {
                sBArray[row] = new StringBuilder(new string(' ', width));
            }
            return sBArray;
        }

        public static void ClearLayer(string layerName) {
            switch (layerName) {
                case "MobLayer": MobLayer = NewBlanckSBArray(MobLayer[0].Length, MobLayer.Length); break;
                case "ItemLayer": ItemLayer = NewBlanckSBArray(ItemLayer[0].Length, ItemLayer.Length); break;
            }
        }

        private static StringBuilder[] GenerateMapLayer(int width, int height) {
            StringBuilder[] sBArray = new StringBuilder[height];

            Random randomNumber = new Random();
            for (int row = 0; row < height; row++) { //adds the possibility of having 2 biomes on one row (could be 1)
                sBArray[row] = new StringBuilder(new string(RandomBiome(randomNumber).Ground, randomNumber.Next(width + 1)));

                sBArray[row].Append(new StringBuilder(new string(RandomBiome(randomNumber).Ground, width - sBArray[row].Length)));
            }
            return sBArray;
        }

        private static Biome RandomBiome(Random randomNumber) {
            Biome currBiome;

            if (randomNumber.Next(4) > 0) { // 3/4 chance that the biome will not hurt and will not suffocate
                currBiome = Biomes.Values.Where(x => x.DamagePoints == 0).ElementAt(randomNumber.Next(Biomes.Values.Count(x => x.DamagePoints == 0)));
            }
            else {
                if (randomNumber.Next(5) > 0) { // 4/5 chance that the biome will hurt and suffocate
                    currBiome = Biomes.Values.Where(x => x.DamagePoints > 0 && x.Suffocate)
                                .ElementAt(randomNumber.Next(Biomes.Values.Count(x => x.DamagePoints > 0 && x.Suffocate)));
                }
                else { // 1/5 chance that the biome will hurt but won't suffocate
                    currBiome = Biomes.Values.Where(x => x.DamagePoints > 0 && !x.Suffocate)
                               .ElementAt(randomNumber.Next(Biomes.Values.Count(x => x.DamagePoints > 0 && !x.Suffocate)));
                }
            }
            return currBiome;
        }

        public static StringBuilder[] World {
            get {
                StringBuilder[] temp = NewBlanckSBArray(MapLayer[0].Length, MapLayer.Length);

                Overlay(temp, MapLayer);
                Overlay(temp, ItemLayer);
                Overlay(temp, MobLayer);

                return temp;
            }
        }

        private static void Overlay(StringBuilder[] origin, StringBuilder[] newLayer) {
            for (int row = 0; row < origin.Length; row++) {
                for (int col = 0; col < origin[0].Length; col++) {
                    if (newLayer[row][col] != ' ') origin[row][col] = newLayer[row][col];
                }
            }
        }

        public static string StringWorld {
            get {
                StringBuilder toReturn = new StringBuilder();
                foreach (var row in World) {
                    toReturn.AppendLine(row.ToString());
                }
                return toReturn.ToString();
            }
        }
    }
}
