﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitspace_2._0.Game_items {
    public class Weapon : Item {
        public Weapon(string name, char body, ConsoleColor color, int health, int maxHealth, int damage) : base(name, body, color, health, maxHealth, damage) {
        }

        public Weapon(string name, char body, ConsoleColor color, int health, int maxHealth, int damage, bool canPlace, bool canWalkOver) : base(name, body, color, health, maxHealth, damage, canPlace, canWalkOver) {
        }
    }
}
