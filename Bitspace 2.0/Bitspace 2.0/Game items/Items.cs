﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitspace_2._0.Game_items {
    static class Items {
        private static Dictionary<string, Weapon> Weapons = new Dictionary<string, Weapon>() {
            { "Stick", new Weapon("Stick", '|', ConsoleColor.Gray, 1, 1, 1) }
        };

        public static List<Weapon> AllWeapons {
            get {
                return new List<Weapon>(Weapons.Values);
            }
        }

        public static List<Item> AllItems {
            get {
                List<Item> toReturn = new List<Item>();

                toReturn.AddRange(AllWeapons);

                return toReturn;
            }
        }
    }
}
