﻿using Bitspace_2._0.Game_creatures;
using System;
using System.Linq;

namespace Bitspace_2._0 {
    public abstract class Item {
        protected string name;
        protected char body;
        protected int initialQuantity;
        public ConsoleColor Color { get; set; }

        public bool CanPlace { get; protected set; }
        public bool CanWalkOver { get; protected set; }

        public int Health { get; set; }
        public int MaxHealth { get; protected set; }
        public int Damage { get; private set; }

        protected Item(string name, char body, ConsoleColor color, int health, int maxHealth, int damage) 
        :this(name, body, color, health, maxHealth, damage, false, false, 0, 0) { }

        protected Item(string name, char body, ConsoleColor color, int health, int maxHealth, int damage, bool canPlace, bool canWalkOver)
        : this(name, body, color, health, maxHealth, damage, canPlace, canWalkOver, 0, 0) { }

        protected Item(string name, char body, ConsoleColor color, int health, int maxHealth, int damage, bool canPlace, bool canWalkOver, int xPos, int yPos) {
            Name = name;
            Body = body;
            Color = color;
            Health = health;
            MaxHealth = maxHealth;
            Damage = damage;
            CanPlace = canPlace;
            CanWalkOver = canWalkOver;
        }

        public void Repair(int repairPoints) {
            this.Health += repairPoints;

            if (this.Health > this.MaxHealth) this.Health = this.MaxHealth;
        }

        public void RecieveDamage(int amount) {
            if (IsNotBroken()) {
                this.Health -= amount;
            }
        }

        public void DoDirectDamage(Mob targetedMob) {
            targetedMob.RecieveDamage(this.Damage);
        }



        public bool IsNotBroken() {
            return this.Health > 0;
        }

        public string Name {
            get { return this.name; }
            protected set {
                if (!value.Any(c => (c > 47 && c < 58) || (c > 64 && c < 91) || (c > 96 && c < 123) || c == ' ')) {
                    throw new ArgumentException("Item name can be composed only of small letters, big letters, numbers and spaces.");
                }
                this.name = value;
            }
        }

        public char Body {
            get { return this.body; }
            protected set {
                if (value < 33 || value > 126) { //TODO: limit it even more (in the end)
                    throw new ArgumentException("Body character can only be from the ASCII printable characters (from 32 to 126), without the space character (32)");
                }
                this.body = value;
            }
        }

        public int Quantity {
            get { return this.initialQuantity; }
            set {
                if (value < 1) {
                    throw new ArgumentException("Quantity of items can't be less than 1");
                }
                this.initialQuantity = value;
            }
        }
    }
}
