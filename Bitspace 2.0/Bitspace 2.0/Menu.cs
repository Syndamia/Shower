﻿using System;
using Bitspace_2._0.Game_map;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitspace_2._0 {
    public static class Menu {
        private static int yPos;

        public static void StartMenu() {
            yPos = 2;
            while (true) {
                Console.Clear();
                Console.CursorVisible = false;

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Bitspace");
                Console.WriteLine();
                Console.WriteLine(" New game");
                Console.WriteLine(" Load game (unavalable)");
                Console.WriteLine(" Settings");

                if (!MenuCursor(2, 4)) break;
            }
            switch (yPos) {
                case 2: ProgressMenu(); MapSizeMenu(); break;
                //case 4: SettingsMenu(); break;
            }
        }

        private static void MapSizeMenu() {
            yPos = 2;
            while (true) {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Choose map size:");
                Console.WriteLine();
                Console.WriteLine(" Small (30 x 10)");
                Console.WriteLine(" Medium (60 x 30)");
                Console.WriteLine(" Large (100 x 40)");

                if (!MenuCursor(2, 4)) break;
            }
            switch (yPos) {
                case 2: Map.Generate(30, 10); break;
                case 3: Map.Generate(60, 30); break;
                case 4: Map.Generate(100, 40); break;
            }

        }

        private static void ProgressMenu() {
            yPos = 2;
            while (true) {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Choose progress saving mode:");
                Console.WriteLine();
                Console.WriteLine(" Get over it! (No saving)");
                Console.WriteLine(" Fanatic (Constant saving) UNAVALABLE");

                if (!MenuCursor(2, 3)) break;
            }
            switch(yPos) {
                case 3:
                case 2: break;
            }
        }

        private static bool MenuCursor(int minValue, int maxValue) { //returns false when enter is pressed
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(0, yPos);
            Console.Write('>');

            var pressedKey = Console.ReadKey().Key;

            if (pressedKey == ConsoleKey.UpArrow && yPos > minValue) yPos--;
            else if (pressedKey == ConsoleKey.DownArrow && yPos < maxValue) yPos++;
            else if (pressedKey == ConsoleKey.Enter) return false;

            return true;
        }
    }
}
