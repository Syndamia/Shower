﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitspace_2._0 {
    public static class Inventory {
        public static string PrintInventory(Item[] items, int selectedItem) {
            StringBuilder toReturn = new StringBuilder();

            toReturn.Append(PrintTopOrBot(true, items, selectedItem));
            toReturn.Append(PrintMid(items, selectedItem));
            toReturn.Append(PrintTopOrBot(false, items, selectedItem));

            return toReturn.ToString();
        }

        private static string PrintMid(Item[] items, int selectedItem) {
            StringBuilder toReturn = new StringBuilder();

            for (int row = 0; row < 2; row++, toReturn.AppendLine()) {
                toReturn.Append("│");

                for (int i = 0; i < items.Length; i++) {
                    switch(row) {
                        case 0:
                            if (items[i] != null) {
                                toReturn.Append(" " + items[i].Body + " │");
                                continue;
                            } break;
                        case 1:
                            if (items[i] != null) {
                                if (items[i].Quantity > 1) {
                                    switch(items[i].Quantity.ToString().Length) {
                                        case 1: toReturn.Append(" " + items[i].Quantity + " │"); break;
                                        case 2: toReturn.Append(" " + items[i].Quantity + "│"); break;
                                        case 3: toReturn.Append(items[i].Quantity + "│"); break;
                                    } continue;
                                }
                            } break;
                    }
                    toReturn.Append("   │");
                }
            }
            return toReturn.ToString();
        }

        private static string PrintTopOrBot(bool printTop, Item[] items, int selectedItem) {
            StringBuilder toReturn = new StringBuilder();

            if (printTop) toReturn.Append("┌");
            else toReturn.Append("└");

            for (int i = 0; i < items.Length ; i++) {
                if (selectedItem == i) {
                    toReturn.Append("***");
                } else {
                    toReturn.Append("───");
                }

                if (i + 1 < items.Length) toReturn.Append("─");
            }

            if (printTop) toReturn.AppendLine("┐");
            else toReturn.AppendLine("┘");

            return toReturn.ToString();
        } 
    }
}
