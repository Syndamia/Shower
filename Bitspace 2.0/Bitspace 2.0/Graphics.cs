﻿using Bitspace_2._0.Game_map;
using Bitspace_2._0.Game_creatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitspace_2._0 {
    static class Graphics {
        public static bool Mono = false;

        public static void ReWriteScreen() {
            Console.SetCursorPosition(0, 0);
            Console.CursorVisible = false;

            UpdateLayers();

            if (Mono) Console.WriteLine(Map.StringWorld);
            else {
                PrintWorld();

                PrintMobInfo();
            }
        }

        private static void PrintWorld() {
            foreach (var row in Map.World) {
                foreach (var character in row.ToString()) {
                    if (Map.Biomes.Any(b => b.Value.Ground == character)) {
                        Console.ForegroundColor = Map.Biomes.Values.First(b => b.Ground == character).Color;
                    }
                    else if (Creatures.AllAliveCreatures.Any(c => c.Body == character)) {
                        Console.ForegroundColor = Creatures.AllAliveCreatures.First(c => c.Body == character).Color;
                    }
                    Console.Write(character);
                }
                Console.WriteLine();
            }
        }

        private static void PrintMobInfo() {
            Console.ForegroundColor = ConsoleColor.Gray;

            foreach(var currPlayer in Creatures.Players.Values) {
                Console.WriteLine(currPlayer);
                Console.WriteLine(currPlayer.PrintInventory());

                foreach(var fightingWithPlayer in Creatures.AllAliveCreatures.Where(c => c.fightingWithPlayer == currPlayer)) {
                    Console.WriteLine("   " + fightingWithPlayer);
                }
            }
        }

        private static void UpdateLayers() {
            UpdateMobLayer();
            //UpdateItemLayer();
        }

        private static void UpdateMobLayer() {
            Map.ClearLayer("MobLayer");

            foreach(var cr in Creatures.AllAliveCreatures) {
                Map.MobLayer[cr.YPos][cr.XPos] = cr.Body;
            }
        }
    }
}
