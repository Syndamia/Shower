# Chopsticks
A personal project; PC transfer of the finger game "chopsticks", mainly made in VB

To start with, just download FingerGame.exe and play it! 

In the folder FingerGame are all the code and files for the game (visual basic/main version). Beware, it is made from Visual Studio 2012!

In the folder cSharpVersion you will find the C# code of the game and .exe. This is a proof of concept, so a lot of functinality of the main game aren't present. To play it download the FingerGameCSharp.exe file. Don't forget to read the "How to play"!
