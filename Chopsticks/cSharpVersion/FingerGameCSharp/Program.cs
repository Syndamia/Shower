﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerGameCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            int p1 = 1;
            int p2 = 1;
            int e1 = 1;
            int e2 = 1;
            int sel = 0;

            for (; ; )
            {               
                if (sel == 0) Console.WriteLine($"{e1}   {e2}\r\n-----\r\n{p1} * {p2}\r\n");
                else Console.WriteLine($"{e1} * {e2}\r\n-----\r\n{p1}   {p2}\r\n");

                char yourHand = Console.ReadLine()[0];
                char toHand = Console.ReadLine()[0];
                Console.WriteLine();

                if (sel == 0)
                {
                    if (yourHand == 'l' && p1 != 5)
                    {
                        if (toHand == 'l' && e1 != 5) e1 += p1;
                        else if (toHand == 'r' && e2 != 5) e2 += p1;
                        sel = 1;
                    }
                    else if (yourHand == 'r' && p2 != 5)
                    {
                        if (toHand == 'l' && e1 != 5) { e1 += p2; sel = 1; }
                        else if (toHand == 'r' && e2 != 5) { e2 += p2; sel = 1; }
                        else Console.WriteLine("Please select valid enemy hand! (You will have to reselect your hand too!)");                        
                    }
                    else
                    {
                        Console.WriteLine("Please select a valid hand!");
                    }                    
                }
                else
                {
                    if (yourHand == 'l' && e1 != 5)
                    {
                        if (toHand == 'l' && p1 != 5) { p1 += e1; sel = 0; }
                        else if (toHand == 'r' && p2 != 5) { p2 += e1; sel = 0; }
                        else Console.WriteLine("Please select valid enemy hand! (You will have to reselect your hand too!)");
                    }
                    else if (yourHand == 'r' && e2 != 5)
                    {
                        if (toHand == 'l' && p1 != 5) { p1 += e2; sel = 0; }
                        else if (toHand == 'r' && p2 != 5) { p2 += e2; sel = 0; }
                        else Console.WriteLine("Please select valid enemy hand! (You will have to reselect your hand too!)");
                    }
                    else
                    {
                        Console.WriteLine("Please select a valid hand!");
                    }

                }

                if (p1 == 5 && p2 == 5)
                {
                    Console.WriteLine("Player 2 wins! New game.");
                    p1 = 1; p2 = 1; e1 = 1; e2 = 1;
                    sel = 0;
                }
                else if (e1 == 5 && e2 == 5)
                {
                    Console.WriteLine("Player 1 wins! New game.");
                    p1 = 1; p2 = 1; e1 = 1; e2 = 1;
                    sel = 1;
                }

                Console.WriteLine();
            }
        }
    }
}
