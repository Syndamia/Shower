﻿Public Class FingerGameForm
    Dim Sel, Hit, N, S As Integer
    Private Sub btnP1_Click(sender As Object, e As EventArgs) Handles btnP1.Click
        If Sel = 0 Then
            btnTrans.Enabled = False
            Hit = Val(lblP1.Text)

            btnP1.Enabled = False : btnP2.Enabled = False : lblP1.Enabled = False : lblP2.Enabled = False
            'decides whether to activate enemy hands or not
            If lblE1.Text = 5 Then
            Else
                btnE1.Enabled = True : lblE1.Enabled = True
            End If

            If lblE2.Text = 5 Then
            Else
                btnE2.Enabled = True : lblE2.Enabled = True
            End If

        Else
            Hit += Val(lblP1.Text)
            If Hit = 5 Then
                lblP1.Text = 5
                btnP1.Image = My.Resources._0
                lblP1.Enabled = False : btnP1.Enabled = False
            Else
                If Hit > 5 Then
                    Hit -= 5
                End If

                lblP1.Text = Hit
                Select Case Hit
                    Case 1
                        btnP1.Image = My.Resources._1
                    Case 2
                        btnP1.Image = My.Resources._2
                    Case 3
                        btnP1.Image = My.Resources._3
                    Case 4
                        btnP1.Image = My.Resources._4
                End Select
            End If
            TurnToP1()
        End If

        WinP2(Val(lblP1.Text), Val(lblP2.Text))
        transferToggle(Val(lblP1.Text), Val(lblP2.Text), Val(lblE1.Text), Val(lblE2.Text))
    End Sub
    Private Sub btnP2_Click(sender As Object, e As EventArgs) Handles btnP2.Click
        If Sel = 0 Then
            btnTrans.Enabled = False
            Hit = Val(lblP2.Text)

            btnP1.Enabled = False : btnP2.Enabled = False : lblP1.Enabled = False : lblP2.Enabled = False

            If lblE1.Text = 5 Then
            Else
                btnE1.Enabled = True : lblE1.Enabled = True
            End If

            If lblE2.Text = 5 Then
            Else
                btnE2.Enabled = True : lblE2.Enabled = True
            End If
        Else
            Hit += Val(lblP2.Text)
            If Hit = 5 Then
                lblP2.Text = 5
                btnP2.Image = My.Resources._0
                lblP2.Enabled = False : btnP2.Enabled = False
            Else
                If Hit > 5 Then
                    Hit -= 5
                End If

                lblP2.Text = Hit
                Select Case Hit
                    Case 1
                        btnP2.Image = My.Resources._1
                    Case 2
                        btnP2.Image = My.Resources._2
                    Case 3
                        btnP2.Image = My.Resources._3
                    Case 4
                        btnP2.Image = My.Resources._4
                End Select
            End If
            TurnToP1()
        End If

        WinP2(Val(lblP1.Text), Val(lblP2.Text))
        transferToggle(Val(lblP1.Text), Val(lblP2.Text), Val(lblE1.Text), Val(lblE2.Text))
    End Sub

    Private Sub btnE1_Click(sender As Object, e As EventArgs) Handles btnE1.Click
        If Sel = 0 Then
            Hit += Val(lblE1.Text)
            If Hit = 5 Then
                lblE1.Text = 5
                btnE1.Image = My.Resources._0
                lblE1.Enabled = False : btnE1.Enabled = False
            Else
                If Hit > 5 Then
                    Hit -= 5
                End If

                lblE1.Text = Hit
                Select Case Hit
                    Case 1
                        btnE1.Image = My.Resources._1d
                    Case 2
                        btnE1.Image = My.Resources._2d
                    Case 3
                        btnE1.Image = My.Resources._3d
                    Case 4
                        btnE1.Image = My.Resources._4d
                End Select
            End If
            TurnToP2()
        Else
            btnTrans.Enabled = False
            Hit = Val(lblE1.Text)
            btnE1.Enabled = False : btnE2.Enabled = False : lblE1.Enabled = False : lblE2.Enabled = False

            If lblP1.Text = 5 Then
            Else
                btnP1.Enabled = True : lblP1.Enabled = True
            End If

            If lblP2.Text = 5 Then
            Else
                btnP2.Enabled = True : lblP2.Enabled = True
            End If

        End If

        WinP1(Val(lblE1.Text), Val(lblE2.Text))
        transferToggle(Val(lblP1.Text), Val(lblP2.Text), Val(lblE1.Text), Val(lblE2.Text))
    End Sub
    Private Sub btnE2_Click(sender As Object, e As EventArgs) Handles btnE2.Click
        If Sel = 0 Then
            Hit += Val(lblE2.Text)
            If Hit = 5 Then
                lblE2.Text = 5
                btnE2.Image = My.Resources._0
                lblE2.Enabled = False : btnE2.Enabled = False
            Else
                If Hit > 5 Then
                    Hit -= 5
                End If

                lblE2.Text = Hit
                Select Case Hit
                    Case 1
                        btnE2.Image = My.Resources._1d
                    Case 2
                        btnE2.Image = My.Resources._2d
                    Case 3
                        btnE2.Image = My.Resources._3d
                    Case 4
                        btnE2.Image = My.Resources._4d
                End Select
            End If
            TurnToP2()
        Else
            btnTrans.Enabled = False
            Hit = Val(lblE2.Text)
            btnE1.Enabled = False : btnE2.Enabled = False : lblE1.Enabled = False : lblE2.Enabled = False

            If lblP1.Text = 5 Then
            Else
                btnP1.Enabled = True : lblP1.Enabled = True
            End If

            If lblP2.Text = 5 Then
            Else
                btnP2.Enabled = True : lblP2.Enabled = True
            End If

        End If

        WinP1(Val(lblE1.Text), Val(lblE2.Text))
        transferToggle(Val(lblP1.Text), Val(lblP2.Text), Val(lblE1.Text), Val(lblE2.Text))
    End Sub

    Private Sub btnStal_Click(sender As Object, e As EventArgs) Handles btnStal.Click
        Dim result As String = MessageBox.Show("Are you sure you want to end the game in stalemate? Nobody will win, and the game will be reset!", "Warning!", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            MsgBox("Nobody wins!", 0, "Game ended")
            S = 1
            WinP2(5, 5)
        End If
    End Sub

    Sub transferToggle(ByVal p1 As Double, ByVal p2 As Double, ByVal e1 As Double, ByVal e2 As Double)

        'detects loop conditions
        If (p1 = 5 Or p2 = 5) And (e1 = 5 Or e2 = 5) Then
            If Sel = 0 And (btnP1.Enabled = True Or btnP2.Enabled = True) Then
                If (p1 = 1 Or p2 = 1) And (e1 = 2 Or e2 = 2) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                ElseIf (p1 = 2 Or p2 = 2) And (e1 = 4 Or e2 = 4) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                ElseIf (p1 = 3 Or p2 = 3) And (e1 = 1 Or e2 = 1) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                ElseIf (p1 = 4 Or p2 = 4) And (e1 = 3 Or e2 = 3) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                Else
                    btnTrans.Enabled = False : lblLOOP.Visible = False
                End If

            ElseIf btnE1.Enabled = True Or btnE2.Enabled = True Then
                If (e1 = 1 Or e2 = 1) And (p1 = 2 Or p2 = 2) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                ElseIf (e1 = 2 Or e2 = 2) And (p1 = 4 Or p2 = 4) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                ElseIf (e1 = 3 Or e2 = 3) And (p1 = 1 Or p2 = 1) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                ElseIf (e1 = 4 Or e2 = 4) And (p1 = 3 Or p2 = 3) Then
                    btnTrans.Enabled = True : lblLOOP.Visible = True
                Else
                    btnTrans.Enabled = False : lblLOOP.Visible = False
                End If
            Else
                btnTrans.Enabled = False : lblLOOP.Visible = False
            End If
        Else
            btnTrans.Enabled = False : lblLOOP.Visible = False
        End If

        'detects free transfer option
        If ((p1 = 5 Or p2 = 5) Or (e1 = 5 Or e2 = 5)) And lblLOOP.Visible = False Then
            If (Sel = 0 And Hit = 0) And lblFreeP1.Visible = True Then
                If p1 = 5 And p2 <> 1 Then
                    btnTrans.Enabled = True
                ElseIf p2 = 5 And p1 <> 1 Then
                    btnTrans.Enabled = True
                End If
            ElseIf lblFreeP2.Visible = True And Hit = 0 Then
                If e1 = 5 And e2 <> 1 Then
                    btnTrans.Enabled = True
                ElseIf e2 = 5 And e1 <> 1 Then
                    btnTrans.Enabled = True
                End If
            Else
                btnTrans.Enabled = False
            End If
        Else
            btnTrans.Enabled = False
        End If
    End Sub
    Private Sub btnTrans_Click(sender As Object, e As EventArgs) Handles btnTrans.Click
        If Sel = 0 Then
            If Val(lblP1.Text) = 5 Then
                If Val(lblP2.Text) = 2 Then
                    lblP1.Text = 1 : lblP2.Text = 1
                    btnP1.Image = My.Resources._1 : btnP2.Image = My.Resources._1
                ElseIf Val(lblP2.Text) = 4 Then
                    lblP1.Text = 2 : lblP2.Text = 2
                    btnP1.Image = My.Resources._2 : btnP2.Image = My.Resources._2
                Else
                    lblP1.Text = 1 : lblP2.Text = 2
                    btnP1.Image = My.Resources._1 : btnP2.Image = My.Resources._2
                End If
            Else
                If Val(lblP1.Text) = 2 Then
                    lblP1.Text = 1 : lblP2.Text = 1
                    btnP1.Image = My.Resources._1 : btnP2.Image = My.Resources._1
                ElseIf Val(lblP1.Text) = 4 Then
                    lblP1.Text = 2 : lblP2.Text = 2
                    btnP1.Image = My.Resources._2 : btnP2.Image = My.Resources._2
                Else
                    lblP1.Text = 2 : lblP2.Text = 1
                    btnP1.Image = My.Resources._2 : btnP2.Image = My.Resources._1
                End If
            End If

            TurnToP2()
        Else
            If Val(lblE1.Text) = 5 Then
                If Val(lblE2.Text) = 2 Then
                    lblE1.Text = 1 : lblE2.Text = 1
                    btnE1.Image = My.Resources._1d : btnE2.Image = My.Resources._1d
                ElseIf Val(lblE2.Text) = 4 Then
                    lblE1.Text = 2 : lblE2.Text = 2
                    btnE1.Image = My.Resources._2d : btnE2.Image = My.Resources._2d
                Else
                    lblE1.Text = 1 : lblE2.Text = 2
                    btnE1.Image = My.Resources._1d : btnE2.Image = My.Resources._2d
                End If
            Else
                If Val(lblE1.Text) = 2 Then
                    lblE1.Text = 1 : lblE2.Text = 1
                    btnE1.Image = My.Resources._1d : btnE2.Image = My.Resources._1d
                ElseIf Val(lblE1.Text) = 4 Then
                    lblE1.Text = 2 : lblE2.Text = 2
                    btnE1.Image = My.Resources._2d : btnE2.Image = My.Resources._2d
                Else
                    lblE1.Text = 2 : lblE2.Text = 1
                    btnE1.Image = My.Resources._2d : btnE2.Image = My.Resources._1d
                End If
            End If

            TurnToP1()
        End If

        If lblLOOP.Visible = True Then
            lblLOOP.Visible = False
        Else
            If Sel = 1 Then
                lblFreeP1.Visible = False
            Else
                lblFreeP2.Visible = False
            End If
        End If

        btnTrans.Enabled = False
        transferToggle(Val(lblP1.Text), Val(lblP2.Text), Val(lblE1.Text), Val(lblE2.Text))
    End Sub

    Private Sub btnNum_Click(sender As Object, e As EventArgs) Handles btnNum.Click
        If N = 0 Then
            lblP1.Visible = True : lblP2.Visible = True : lblE1.Visible = True : lblE2.Visible = True
            N = 1
        Else
            lblP1.Visible = False : lblP2.Visible = False : lblE1.Visible = False : lblE2.Visible = False
            N = 0
        End If
    End Sub
    Private Sub btnInfo_Click(sender As Object, e As EventArgs) Handles btnInfo.Click
        MsgBox("   Every player has 2 hands. Click on one hand to select it and then click on enemy hand to add that much 'fingers' to the enemy hand. The goal is to make the enemy hand have 5 fingers, then it is disqualified from the game." & Environment.NewLine & "   The circle shows who's turn it is." & Environment.NewLine & "   The transfer button, transfers half of fingers from one hand to the other disqualified hand (that gives fingers to the disqualified, making it playable again), every player has 1 free use. In special cases of looping (endless finger giving) everyone can use a transfer for free." & Environment.NewLine & Environment.NewLine & "Game code and design by Kamen Dimitrov Mladenov. This is a personal project that transfers of the game 'Chopsticks' to PC, not a commercial product. Made 10-18 January 2019" & Environment.NewLine & Environment.NewLine & "   Finger gestures photos: http://www.nipic.com/show/17256965.html" & Environment.NewLine & "   Your turn icon found on: https://usefulleader.com/2017/12/18/avoid-an-its-your-turn-justification-for-decisions/", 0, "Info")
    End Sub

    Sub WinP1(ByVal a As Integer, ByVal b As Integer)
        If a = 5 And b = 5 Then
            MsgBox("Player 1 wins!", 0, "Game ended")
            lblFreeP1.Visible = True : lblFreeP2.Visible = True : lblLOOP.Visible = False
            lblP1.Text = 1 : lblP2.Text = 1 : lblE1.Text = 1 : lblE2.Text = 1
            btnP1.Image = My.Resources._1 : btnP2.Image = My.Resources._1 : btnE1.Image = My.Resources._1d : btnE2.Image = My.Resources._1d
            TurnToP2()
        End If

    End Sub
    Sub WinP2(ByVal a As Integer, ByVal b As Integer)
        If a = 5 And b = 5 Then
            lblFreeP1.Visible = True : lblFreeP2.Visible = True : lblLOOP.Visible = False
            lblP1.Text = 1 : lblP2.Text = 1 : lblE1.Text = 1 : lblE2.Text = 1
            btnP1.Image = My.Resources._1 : btnP2.Image = My.Resources._1 : btnE1.Image = My.Resources._1d : btnE2.Image = My.Resources._1d
            TurnToP1()
            If S = 0 Then
                MsgBox("Player 2 wins!", 0, "Game ended")
            Else
                S = 0
            End If
        End If
    End Sub

    Sub TurnToP1()
        Sel = 0 : Hit = 0
        ovalP.Visible = True : ovalE.Visible = False
        btnE1.Enabled = False : btnE2.Enabled = False : lblE1.Enabled = False : lblE2.Enabled = False
        If lblP1.Text <> 5 Then
            btnP1.Enabled = True : lblP1.Enabled = True
        End If
        If lblP2.Text <> 5 Then
            btnP2.Enabled = True : lblP2.Enabled = True
        End If
    End Sub
    Sub TurnToP2()
        Sel = 1 : Hit = 0
        ovalP.Visible = False : ovalE.Visible = True
        btnP1.Enabled = False : btnP2.Enabled = False : lblP1.Enabled = False : lblP2.Enabled = False
        If lblE1.Text <> 5 Then
            btnE1.Enabled = True : lblE1.Enabled = True
        End If
        If lblE2.Text <> 5 Then
            btnE2.Enabled = True : lblE2.Enabled = True
        End If
    End Sub
    
End Class
