﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FingerGameForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FingerGameForm))
        Me.lblE1 = New System.Windows.Forms.Label()
        Me.lblE2 = New System.Windows.Forms.Label()
        Me.lblP1 = New System.Windows.Forms.Label()
        Me.lblP2 = New System.Windows.Forms.Label()
        Me.btnStal = New System.Windows.Forms.Button()
        Me.btnTrans = New System.Windows.Forms.Button()
        Me.btnNum = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnInfo = New System.Windows.Forms.Button()
        Me.ovalE = New System.Windows.Forms.PictureBox()
        Me.ovalP = New System.Windows.Forms.PictureBox()
        Me.btnP2 = New System.Windows.Forms.Button()
        Me.btnE2 = New System.Windows.Forms.Button()
        Me.btnE1 = New System.Windows.Forms.Button()
        Me.btnP1 = New System.Windows.Forms.Button()
        Me.lblFreeP2 = New System.Windows.Forms.Label()
        Me.lblFreeP1 = New System.Windows.Forms.Label()
        Me.lblLOOP = New System.Windows.Forms.Label()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        CType(Me.ovalE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ovalP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblE1
        '
        Me.lblE1.AutoSize = True
        Me.lblE1.BackColor = System.Drawing.Color.Transparent
        Me.lblE1.Enabled = False
        Me.lblE1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblE1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.lblE1.Location = New System.Drawing.Point(57, 91)
        Me.lblE1.Name = "lblE1"
        Me.lblE1.Size = New System.Drawing.Size(29, 31)
        Me.lblE1.TabIndex = 4
        Me.lblE1.Text = "1"
        Me.lblE1.Visible = False
        '
        'lblE2
        '
        Me.lblE2.AutoSize = True
        Me.lblE2.BackColor = System.Drawing.Color.Transparent
        Me.lblE2.Enabled = False
        Me.lblE2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblE2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.lblE2.Location = New System.Drawing.Point(228, 90)
        Me.lblE2.Name = "lblE2"
        Me.lblE2.Size = New System.Drawing.Size(29, 31)
        Me.lblE2.TabIndex = 5
        Me.lblE2.Text = "1"
        Me.lblE2.Visible = False
        '
        'lblP1
        '
        Me.lblP1.AutoSize = True
        Me.lblP1.BackColor = System.Drawing.Color.Transparent
        Me.lblP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblP1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.lblP1.Location = New System.Drawing.Point(16, 257)
        Me.lblP1.Name = "lblP1"
        Me.lblP1.Size = New System.Drawing.Size(29, 31)
        Me.lblP1.TabIndex = 6
        Me.lblP1.Text = "1"
        Me.lblP1.Visible = False
        '
        'lblP2
        '
        Me.lblP2.AutoSize = True
        Me.lblP2.BackColor = System.Drawing.Color.Transparent
        Me.lblP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblP2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.lblP2.Location = New System.Drawing.Point(185, 256)
        Me.lblP2.Name = "lblP2"
        Me.lblP2.Size = New System.Drawing.Size(29, 31)
        Me.lblP2.TabIndex = 7
        Me.lblP2.Text = "1"
        Me.lblP2.Visible = False
        '
        'btnStal
        '
        Me.btnStal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStal.Location = New System.Drawing.Point(89, 145)
        Me.btnStal.Name = "btnStal"
        Me.btnStal.Size = New System.Drawing.Size(94, 39)
        Me.btnStal.TabIndex = 9
        Me.btnStal.Text = "Stalemate"
        Me.btnStal.UseVisualStyleBackColor = True
        '
        'btnTrans
        '
        Me.btnTrans.Enabled = False
        Me.btnTrans.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTrans.Location = New System.Drawing.Point(89, 197)
        Me.btnTrans.Name = "btnTrans"
        Me.btnTrans.Size = New System.Drawing.Size(94, 39)
        Me.btnTrans.TabIndex = 12
        Me.btnTrans.Text = "Transfer"
        Me.btnTrans.UseVisualStyleBackColor = True
        '
        'btnNum
        '
        Me.btnNum.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum.ForeColor = System.Drawing.Color.DodgerBlue
        Me.btnNum.Location = New System.Drawing.Point(201, 171)
        Me.btnNum.Name = "btnNum"
        Me.btnNum.Size = New System.Drawing.Size(40, 39)
        Me.btnNum.TabIndex = 13
        Me.btnNum.Text = "Num"
        Me.btnNum.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(113, 357)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 15)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Player 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(113, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Player 2"
        '
        'btnInfo
        '
        Me.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInfo.ForeColor = System.Drawing.Color.DodgerBlue
        Me.btnInfo.Location = New System.Drawing.Point(31, 171)
        Me.btnInfo.Name = "btnInfo"
        Me.btnInfo.Size = New System.Drawing.Size(40, 39)
        Me.btnInfo.TabIndex = 16
        Me.btnInfo.Text = "Info"
        Me.btnInfo.UseVisualStyleBackColor = True
        '
        'ovalE
        '
        Me.ovalE.BackColor = System.Drawing.Color.Transparent
        Me.ovalE.Image = Global.FingerGame.My.Resources.Resources.your_turn_17
        Me.ovalE.Location = New System.Drawing.Point(98, 36)
        Me.ovalE.Name = "ovalE"
        Me.ovalE.Size = New System.Drawing.Size(77, 89)
        Me.ovalE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.ovalE.TabIndex = 11
        Me.ovalE.TabStop = False
        Me.ovalE.Visible = False
        '
        'ovalP
        '
        Me.ovalP.BackColor = System.Drawing.Color.Transparent
        Me.ovalP.Image = Global.FingerGame.My.Resources.Resources.your_turn_17
        Me.ovalP.Location = New System.Drawing.Point(98, 256)
        Me.ovalP.Name = "ovalP"
        Me.ovalP.Size = New System.Drawing.Size(77, 89)
        Me.ovalP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.ovalP.TabIndex = 10
        Me.ovalP.TabStop = False
        '
        'btnP2
        '
        Me.btnP2.BackColor = System.Drawing.Color.Transparent
        Me.btnP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnP2.ForeColor = System.Drawing.Color.Transparent
        Me.btnP2.Image = Global.FingerGame.My.Resources.Resources._1
        Me.btnP2.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnP2.Location = New System.Drawing.Point(182, 254)
        Me.btnP2.Name = "btnP2"
        Me.btnP2.Size = New System.Drawing.Size(78, 125)
        Me.btnP2.TabIndex = 3
        Me.btnP2.UseVisualStyleBackColor = False
        '
        'btnE2
        '
        Me.btnE2.BackColor = System.Drawing.Color.Transparent
        Me.btnE2.Enabled = False
        Me.btnE2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnE2.ForeColor = System.Drawing.Color.Transparent
        Me.btnE2.Image = Global.FingerGame.My.Resources.Resources._1d
        Me.btnE2.Location = New System.Drawing.Point(182, 0)
        Me.btnE2.Name = "btnE2"
        Me.btnE2.Size = New System.Drawing.Size(78, 125)
        Me.btnE2.TabIndex = 1
        Me.btnE2.UseVisualStyleBackColor = False
        '
        'btnE1
        '
        Me.btnE1.BackColor = System.Drawing.Color.Transparent
        Me.btnE1.Enabled = False
        Me.btnE1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnE1.ForeColor = System.Drawing.Color.Transparent
        Me.btnE1.Image = Global.FingerGame.My.Resources.Resources._1d
        Me.btnE1.Location = New System.Drawing.Point(12, 0)
        Me.btnE1.Name = "btnE1"
        Me.btnE1.Size = New System.Drawing.Size(78, 125)
        Me.btnE1.TabIndex = 0
        Me.btnE1.UseVisualStyleBackColor = False
        '
        'btnP1
        '
        Me.btnP1.BackColor = System.Drawing.Color.Transparent
        Me.btnP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnP1.ForeColor = System.Drawing.Color.Transparent
        Me.btnP1.Image = Global.FingerGame.My.Resources.Resources._1
        Me.btnP1.Location = New System.Drawing.Point(12, 253)
        Me.btnP1.Name = "btnP1"
        Me.btnP1.Size = New System.Drawing.Size(78, 125)
        Me.btnP1.TabIndex = 2
        Me.btnP1.UseVisualStyleBackColor = False
        '
        'lblFreeP2
        '
        Me.lblFreeP2.AutoSize = True
        Me.lblFreeP2.ForeColor = System.Drawing.Color.Red
        Me.lblFreeP2.Location = New System.Drawing.Point(99, 129)
        Me.lblFreeP2.Name = "lblFreeP2"
        Me.lblFreeP2.Size = New System.Drawing.Size(75, 13)
        Me.lblFreeP2.TabIndex = 17
        Me.lblFreeP2.Text = "1 Free transfer"
        '
        'lblFreeP1
        '
        Me.lblFreeP1.AutoSize = True
        Me.lblFreeP1.ForeColor = System.Drawing.Color.Blue
        Me.lblFreeP1.Location = New System.Drawing.Point(99, 239)
        Me.lblFreeP1.Name = "lblFreeP1"
        Me.lblFreeP1.Size = New System.Drawing.Size(75, 13)
        Me.lblFreeP1.TabIndex = 18
        Me.lblFreeP1.Text = "1 Free transfer"
        '
        'lblLOOP
        '
        Me.lblLOOP.AutoSize = True
        Me.lblLOOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLOOP.Location = New System.Drawing.Point(116, 184)
        Me.lblLOOP.Name = "lblLOOP"
        Me.lblLOOP.Size = New System.Drawing.Size(40, 13)
        Me.lblLOOP.TabIndex = 19
        Me.lblLOOP.Text = "LOOP"
        Me.lblLOOP.Visible = False
        '
        'Timer
        '
        Me.Timer.Interval = 1000
        '
        'FingerGameForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(272, 381)
        Me.Controls.Add(Me.lblLOOP)
        Me.Controls.Add(Me.lblFreeP1)
        Me.Controls.Add(Me.lblFreeP2)
        Me.Controls.Add(Me.btnInfo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnNum)
        Me.Controls.Add(Me.btnTrans)
        Me.Controls.Add(Me.ovalE)
        Me.Controls.Add(Me.ovalP)
        Me.Controls.Add(Me.btnStal)
        Me.Controls.Add(Me.lblP2)
        Me.Controls.Add(Me.lblP1)
        Me.Controls.Add(Me.lblE2)
        Me.Controls.Add(Me.lblE1)
        Me.Controls.Add(Me.btnP2)
        Me.Controls.Add(Me.btnE2)
        Me.Controls.Add(Me.btnE1)
        Me.Controls.Add(Me.btnP1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FingerGameForm"
        Me.Text = "Chopsticks (finger game)"
        CType(Me.ovalE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ovalP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnE1 As System.Windows.Forms.Button
    Friend WithEvents btnE2 As System.Windows.Forms.Button
    Friend WithEvents btnP2 As System.Windows.Forms.Button
    Friend WithEvents lblE1 As System.Windows.Forms.Label
    Friend WithEvents lblE2 As System.Windows.Forms.Label
    Friend WithEvents lblP1 As System.Windows.Forms.Label
    Friend WithEvents lblP2 As System.Windows.Forms.Label
    Friend WithEvents btnStal As System.Windows.Forms.Button
    Friend WithEvents btnP1 As System.Windows.Forms.Button
    Friend WithEvents ovalP As System.Windows.Forms.PictureBox
    Friend WithEvents ovalE As System.Windows.Forms.PictureBox
    Friend WithEvents btnTrans As System.Windows.Forms.Button
    Friend WithEvents btnNum As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnInfo As System.Windows.Forms.Button
    Friend WithEvents lblFreeP2 As System.Windows.Forms.Label
    Friend WithEvents lblFreeP1 As System.Windows.Forms.Label
    Friend WithEvents lblLOOP As System.Windows.Forms.Label
    Friend WithEvents Timer As System.Windows.Forms.Timer

End Class
